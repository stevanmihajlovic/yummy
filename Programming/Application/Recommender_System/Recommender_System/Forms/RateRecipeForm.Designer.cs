﻿namespace Recommender_System.Forms
{
    partial class RateRecipeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxEmpty5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEmpty4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEmpty1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEmpty2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxEmpty3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFull5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFull4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFull3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFull2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFull1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxEmpty5
            // 
            this.pictureBoxEmpty5.Image = global::Recommender_System.Properties.Resources.empty_star;
            this.pictureBoxEmpty5.Location = new System.Drawing.Point(196, 12);
            this.pictureBoxEmpty5.Name = "pictureBoxEmpty5";
            this.pictureBoxEmpty5.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxEmpty5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpty5.TabIndex = 21;
            this.pictureBoxEmpty5.TabStop = false;
            this.pictureBoxEmpty5.MouseEnter += new System.EventHandler(this.pictureBoxEmpty5_MouseEnter);
            // 
            // pictureBoxEmpty4
            // 
            this.pictureBoxEmpty4.Image = global::Recommender_System.Properties.Resources.empty_star;
            this.pictureBoxEmpty4.Location = new System.Drawing.Point(150, 12);
            this.pictureBoxEmpty4.Name = "pictureBoxEmpty4";
            this.pictureBoxEmpty4.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxEmpty4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpty4.TabIndex = 20;
            this.pictureBoxEmpty4.TabStop = false;
            this.pictureBoxEmpty4.MouseEnter += new System.EventHandler(this.pictureBoxEmpty4_MouseEnter);
            // 
            // pictureBoxEmpty1
            // 
            this.pictureBoxEmpty1.Image = global::Recommender_System.Properties.Resources.empty_star;
            this.pictureBoxEmpty1.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxEmpty1.Name = "pictureBoxEmpty1";
            this.pictureBoxEmpty1.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxEmpty1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpty1.TabIndex = 17;
            this.pictureBoxEmpty1.TabStop = false;
            this.pictureBoxEmpty1.MouseEnter += new System.EventHandler(this.pictureBoxEmpty1_MouseEnter);
            // 
            // pictureBoxEmpty2
            // 
            this.pictureBoxEmpty2.Image = global::Recommender_System.Properties.Resources.empty_star;
            this.pictureBoxEmpty2.Location = new System.Drawing.Point(58, 12);
            this.pictureBoxEmpty2.Name = "pictureBoxEmpty2";
            this.pictureBoxEmpty2.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxEmpty2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpty2.TabIndex = 18;
            this.pictureBoxEmpty2.TabStop = false;
            this.pictureBoxEmpty2.MouseEnter += new System.EventHandler(this.pictureBoxEmpty2_MouseEnter);
            // 
            // pictureBoxEmpty3
            // 
            this.pictureBoxEmpty3.Image = global::Recommender_System.Properties.Resources.empty_star;
            this.pictureBoxEmpty3.Location = new System.Drawing.Point(104, 12);
            this.pictureBoxEmpty3.Name = "pictureBoxEmpty3";
            this.pictureBoxEmpty3.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxEmpty3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmpty3.TabIndex = 19;
            this.pictureBoxEmpty3.TabStop = false;
            this.pictureBoxEmpty3.MouseEnter += new System.EventHandler(this.pictureBoxEmpty3_MouseEnter);
            // 
            // pictureBoxFull5
            // 
            this.pictureBoxFull5.Image = global::Recommender_System.Properties.Resources.full_star;
            this.pictureBoxFull5.Location = new System.Drawing.Point(196, 12);
            this.pictureBoxFull5.Name = "pictureBoxFull5";
            this.pictureBoxFull5.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxFull5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFull5.TabIndex = 26;
            this.pictureBoxFull5.TabStop = false;
            this.pictureBoxFull5.Visible = false;
            this.pictureBoxFull5.Click += new System.EventHandler(this.pictureBoxFull5_Click);
            this.pictureBoxFull5.MouseLeave += new System.EventHandler(this.pictureBoxFull5_MouseLeave);
            // 
            // pictureBoxFull4
            // 
            this.pictureBoxFull4.Image = global::Recommender_System.Properties.Resources.full_star;
            this.pictureBoxFull4.Location = new System.Drawing.Point(150, 12);
            this.pictureBoxFull4.Name = "pictureBoxFull4";
            this.pictureBoxFull4.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxFull4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFull4.TabIndex = 25;
            this.pictureBoxFull4.TabStop = false;
            this.pictureBoxFull4.Visible = false;
            this.pictureBoxFull4.Click += new System.EventHandler(this.pictureBoxFull4_Click);
            this.pictureBoxFull4.MouseLeave += new System.EventHandler(this.pictureBoxFull4_MouseLeave);
            // 
            // pictureBoxFull3
            // 
            this.pictureBoxFull3.Image = global::Recommender_System.Properties.Resources.full_star;
            this.pictureBoxFull3.Location = new System.Drawing.Point(104, 12);
            this.pictureBoxFull3.Name = "pictureBoxFull3";
            this.pictureBoxFull3.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxFull3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFull3.TabIndex = 24;
            this.pictureBoxFull3.TabStop = false;
            this.pictureBoxFull3.Visible = false;
            this.pictureBoxFull3.Click += new System.EventHandler(this.pictureBoxFull3_Click);
            this.pictureBoxFull3.MouseLeave += new System.EventHandler(this.pictureBoxFull3_MouseLeave);
            // 
            // pictureBoxFull2
            // 
            this.pictureBoxFull2.Image = global::Recommender_System.Properties.Resources.full_star;
            this.pictureBoxFull2.Location = new System.Drawing.Point(58, 12);
            this.pictureBoxFull2.Name = "pictureBoxFull2";
            this.pictureBoxFull2.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxFull2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFull2.TabIndex = 23;
            this.pictureBoxFull2.TabStop = false;
            this.pictureBoxFull2.Visible = false;
            this.pictureBoxFull2.Click += new System.EventHandler(this.pictureBoxFull2_Click);
            this.pictureBoxFull2.MouseLeave += new System.EventHandler(this.pictureBoxFull2_MouseLeave);
            // 
            // pictureBoxFull1
            // 
            this.pictureBoxFull1.Image = global::Recommender_System.Properties.Resources.full_star;
            this.pictureBoxFull1.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxFull1.Name = "pictureBoxFull1";
            this.pictureBoxFull1.Size = new System.Drawing.Size(40, 36);
            this.pictureBoxFull1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFull1.TabIndex = 22;
            this.pictureBoxFull1.TabStop = false;
            this.pictureBoxFull1.Visible = false;
            this.pictureBoxFull1.Click += new System.EventHandler(this.pictureBoxFull1_Click);
            this.pictureBoxFull1.MouseLeave += new System.EventHandler(this.pictureBoxFull1_MouseLeave);
            // 
            // RateRecipeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 60);
            this.Controls.Add(this.pictureBoxEmpty5);
            this.Controls.Add(this.pictureBoxEmpty4);
            this.Controls.Add(this.pictureBoxEmpty3);
            this.Controls.Add(this.pictureBoxEmpty2);
            this.Controls.Add(this.pictureBoxEmpty1);
            this.Controls.Add(this.pictureBoxFull5);
            this.Controls.Add(this.pictureBoxFull4);
            this.Controls.Add(this.pictureBoxFull3);
            this.Controls.Add(this.pictureBoxFull2);
            this.Controls.Add(this.pictureBoxFull1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "RateRecipeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmpty3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxEmpty5;
        private System.Windows.Forms.PictureBox pictureBoxEmpty4;
        private System.Windows.Forms.PictureBox pictureBoxEmpty1;
        private System.Windows.Forms.PictureBox pictureBoxEmpty2;
        private System.Windows.Forms.PictureBox pictureBoxEmpty3;
        private System.Windows.Forms.PictureBox pictureBoxFull5;
        private System.Windows.Forms.PictureBox pictureBoxFull4;
        private System.Windows.Forms.PictureBox pictureBoxFull3;
        private System.Windows.Forms.PictureBox pictureBoxFull2;
        private System.Windows.Forms.PictureBox pictureBoxFull1;
    }
}