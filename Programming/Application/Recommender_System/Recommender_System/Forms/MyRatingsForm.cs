﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class MyRatingsForm : Form
    {
        GraphClient client;
        User user;
        List<Recipe> recipes;
        List<int> ratings;
        int index = 0;

        public MyRatingsForm(GraphClient client, User user)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;

            buttonPrevious.Visible = false;

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("id_user", user.username);
            var query = new CypherQuery("MATCH n-[:rated]->m WHERE n.username = {id_user} RETURN m", dictionary, CypherResultMode.Set);
            recipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();

            query = new CypherQuery("MATCH n-[r:rated]->m WHERE n.username = {id_user} RETURN r.rate", dictionary, CypherResultMode.Set);
            ratings = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).ToList();

            if (index < recipes.Count)
            {
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelRating1.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe1.Image = null;
                labelName1.Text = "";
                labelRating1.Text = "";
                labelRatingTitle1.Visible = false;
                buttonChange1.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelRating2.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe2.Image = null;
                labelName2.Text = "";
                labelRating2.Text = "";
                labelRatingTitle2.Visible = false;
                buttonChange2.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelRating3.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe3.Image = null;
                labelName3.Text = "";
                labelRating3.Text = "";
                labelRatingTitle3.Visible = false;
                buttonChange3.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelRating4.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe4.Image = null;
                labelName4.Text = "";
                labelRating4.Text = "";
                labelRatingTitle4.Visible = false;
                buttonChange4.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe5.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName5.Text = recipes[index].name;
                labelRating5.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe5.Image = null;
                labelName5.Text = "";
                labelRating5.Text = "";
                labelRatingTitle5.Visible = false;
                buttonChange5.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe6.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName6.Text = recipes[index].name;
                labelRating6.Text = ratings[index].ToString();
                index++;
            }
            else
            {
                pictureBoxRecipe6.Image = null;
                labelName6.Text = "";
                labelRating6.Text = "";
                labelRatingTitle6.Visible = false;
                buttonChange6.Visible = false;
            }

            if (recipes.Count <= 6)
                buttonNext.Visible = false;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (labelName1.Text == "" || labelName2.Text == "" || labelName3.Text == "" || labelName4.Text == "" || labelName5.Text == "" || labelName6.Text == "")
                return;

            if (index < recipes.Count)
            {
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelRating1.Text = ratings[index].ToString();
                labelRatingTitle1.Visible = true;
                buttonChange1.Visible = true;
                buttonPrevious.Visible = true;
                index++;
            }
            else
                return;

            if (index < recipes.Count)
            {
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelRating2.Text = ratings[index].ToString();
                labelRatingTitle2.Visible = true;
                buttonChange2.Visible = true;
                index++;
            }
            else
            {
                pictureBoxRecipe2.Image = null;
                labelName2.Text = "";
                labelRating2.Text = "";
                labelRatingTitle2.Visible = false;
                buttonChange2.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelRating3.Text = ratings[index].ToString();
                labelRatingTitle3.Visible = true;
                buttonChange3.Visible = true;
                index++;
            }
            else
            {
                pictureBoxRecipe3.Image = null;
                labelName3.Text = "";
                labelRating3.Text = "";
                labelRatingTitle3.Visible = false;
                buttonChange3.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelRating4.Text = ratings[index].ToString();
                labelRatingTitle4.Visible = true;
                buttonChange4.Visible = true;
                index++;
            }
            else
            {
                pictureBoxRecipe4.Image = null;
                labelName4.Text = "";
                labelRating4.Text = "";
                labelRatingTitle4.Visible = false;
                buttonChange4.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe5.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName5.Text = recipes[index].name;
                labelRating5.Text = ratings[index].ToString();
                labelRatingTitle5.Visible = true;
                buttonChange5.Visible = true;
                index++;
            }
            else
            {
                pictureBoxRecipe5.Image = null;
                labelName5.Text = "";
                labelRating5.Text = "";
                labelRatingTitle5.Visible = false;
                buttonChange5.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe6.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName6.Text = recipes[index].name;
                labelRating6.Text = ratings[index].ToString();
                labelRatingTitle6.Visible = true;
                buttonChange6.Visible = true;
                index++;
            }
            else
            {
                pictureBoxRecipe6.Image = null;
                labelName6.Text = "";
                labelRating6.Text = "";
                labelRatingTitle6.Visible = false;
                buttonChange6.Visible = false;
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
            }
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            buttonNext.Visible = true;
            if (recipes.Count <= 6)
                return;
            while (index % 6 != 0)
                index++;
            if (index > 6)
                index -= 6;

            if (index > 0)
            {
                index--;
                pictureBoxRecipe6.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName6.Text = recipes[index].name;
                labelRating6.Text = ratings[index].ToString();
                labelRatingTitle6.Visible = true;
                buttonChange6.Visible = true;
            }
            else
            {
                pictureBoxRecipe6.Image = null;
                labelName6.Text = "";
                labelRating6.Text = "";
                labelRatingTitle6.Visible = false;
                buttonChange6.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe5.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName5.Text = recipes[index].name;
                labelRating5.Text = ratings[index].ToString();
                labelRatingTitle5.Visible = true;
                buttonChange5.Visible = true;
            }
            else
            {
                pictureBoxRecipe5.Image = null;
                labelName5.Text = "";
                labelRating5.Text = "";
                labelRatingTitle5.Visible = false;
                buttonChange5.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelRating4.Text = ratings[index].ToString();
                labelRatingTitle4.Visible = true;
                buttonChange4.Visible = true;
            }
            else
            {
                pictureBoxRecipe4.Image = null;
                labelName4.Text = "";
                labelRating4.Text = "";
                labelRatingTitle4.Visible = false;
                buttonChange4.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelRating3.Text = ratings[index].ToString();
                labelRatingTitle3.Visible = true;
                buttonChange3.Visible = true;
            }
            else
            {
                pictureBoxRecipe3.Image = null;
                labelName3.Text = "";
                labelRating3.Text = "";
                labelRatingTitle3.Visible = false;
                buttonChange3.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelRating2.Text = ratings[index].ToString();
                labelRatingTitle2.Visible = true;
                buttonChange2.Visible = true;
            }
            else
            {
                pictureBoxRecipe2.Image = null;
                labelName2.Text = "";
                labelRating2.Text = "";
                labelRatingTitle2.Visible = false;
                buttonChange2.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelRating1.Text = ratings[index].ToString();
                labelRatingTitle1.Visible = true;
                buttonChange1.Visible = true;
            }
            else
            {
                pictureBoxRecipe1.Image = null;
                labelName1.Text = "";
                labelRating1.Text = "";
                labelRatingTitle1.Visible = false;
                buttonChange1.Visible = false;
            }
            index += 6;
            if (index == 6)
                buttonPrevious.Visible = false;
        }

        private void buttonChange1_Click(object sender, EventArgs e)
        {
            int i;
            if (labelName6.Text != String.Empty)
                i = index - 6;
            else if (labelName5.Text != String.Empty)
                i = index - 5;
            else if (labelName4.Text != String.Empty)
                i = index - 4;
            else if (labelName3.Text != String.Empty)
                i = index - 3;
            else if (labelName2.Text != String.Empty)
                i = index - 2;
            else
                i = index - 1;

            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating1.Text = ratings[i].ToString();
        }

        private void buttonChange2_Click(object sender, EventArgs e)
        {
            int i;
            if (labelName6.Text != String.Empty)
                i = index - 5;
            else if (labelName5.Text != String.Empty)
                i = index - 4;
            else if (labelName4.Text != String.Empty)
                i = index - 3;
            else if (labelName3.Text != String.Empty)
                i = index - 2;
            else
                i = index - 1;

            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating2.Text = ratings[i].ToString();
        }

        private void buttonChange3_Click(object sender, EventArgs e)
        {
            int i;
            if (labelName6.Text != String.Empty)
                i = index - 4;
            else if (labelName5.Text != String.Empty)
                i = index - 3;
            else if (labelName4.Text != String.Empty)
                i = index - 2;
            else
                i = index - 1;

            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating3.Text = ratings[i].ToString();
        }

        private void buttonChange4_Click(object sender, EventArgs e)
        {
            int i;
            if (labelName6.Text != String.Empty)
                i = index - 3;
            else if (labelName5.Text != String.Empty)
                i = index - 2;
            else
                i = index - 1;

            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating4.Text = ratings[i].ToString();
        }

        private void buttonChange5_Click(object sender, EventArgs e)
        {
            int i;
            if (labelName6.Text != String.Empty)
                i = index - 2;
            else
                i = index - 1;

            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating5.Text = ratings[i].ToString();
        }

        private void buttonChange6_Click(object sender, EventArgs e)
        {
            int i = index - 1;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[i]);
            form.ShowDialog();
            ratings[i] = form.stars;
            labelRating6.Text = ratings[i].ToString();
        }
    }
}