﻿namespace Recommender_System.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.labelUser = new System.Windows.Forms.Label();
            this.buttonSearchRecipes = new System.Windows.Forms.Button();
            this.groupBoxRecipe4 = new System.Windows.Forms.GroupBox();
            this.labelType4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelName4 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe4 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe3 = new System.Windows.Forms.GroupBox();
            this.labelType3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelName3 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe3 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe2 = new System.Windows.Forms.GroupBox();
            this.labelType2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelName2 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe2 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe1 = new System.Windows.Forms.GroupBox();
            this.labelType1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelName1 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe1 = new System.Windows.Forms.PictureBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.pictureBoxAvatar = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).BeginInit();
            this.groupBoxRecipe3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).BeginInit();
            this.groupBoxRecipe2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).BeginInit();
            this.groupBoxRecipe1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(203, 17);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(127, 29);
            this.labelUser.TabIndex = 17;
            this.labelUser.Text = "Welcome, ";
            // 
            // buttonSearchRecipes
            // 
            this.buttonSearchRecipes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchRecipes.Location = new System.Drawing.Point(12, 12);
            this.buttonSearchRecipes.Name = "buttonSearchRecipes";
            this.buttonSearchRecipes.Size = new System.Drawing.Size(173, 37);
            this.buttonSearchRecipes.TabIndex = 12;
            this.buttonSearchRecipes.TabStop = false;
            this.buttonSearchRecipes.Text = "Search Recipes";
            this.buttonSearchRecipes.UseVisualStyleBackColor = true;
            this.buttonSearchRecipes.Click += new System.EventHandler(this.buttonSearchRecipes_Click);
            // 
            // groupBoxRecipe4
            // 
            this.groupBoxRecipe4.Controls.Add(this.labelType4);
            this.groupBoxRecipe4.Controls.Add(this.label16);
            this.groupBoxRecipe4.Controls.Add(this.label17);
            this.groupBoxRecipe4.Controls.Add(this.labelName4);
            this.groupBoxRecipe4.Controls.Add(this.pictureBoxRecipe4);
            this.groupBoxRecipe4.Location = new System.Drawing.Point(398, 264);
            this.groupBoxRecipe4.Name = "groupBoxRecipe4";
            this.groupBoxRecipe4.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe4.TabIndex = 22;
            this.groupBoxRecipe4.TabStop = false;
            // 
            // labelType4
            // 
            this.labelType4.AutoSize = true;
            this.labelType4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType4.Location = new System.Drawing.Point(148, 152);
            this.labelType4.Name = "labelType4";
            this.labelType4.Size = new System.Drawing.Size(51, 20);
            this.labelType4.TabIndex = 4;
            this.labelType4.Text = "label1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(147, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 24);
            this.label16.TabIndex = 3;
            this.label16.Text = "Type:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(147, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 24);
            this.label17.TabIndex = 2;
            this.label17.Text = "Name:";
            // 
            // labelName4
            // 
            this.labelName4.AutoSize = true;
            this.labelName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName4.Location = new System.Drawing.Point(148, 59);
            this.labelName4.Name = "labelName4";
            this.labelName4.Size = new System.Drawing.Size(51, 20);
            this.labelName4.TabIndex = 1;
            this.labelName4.Text = "label1";
            // 
            // pictureBoxRecipe4
            // 
            this.pictureBoxRecipe4.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe4.Name = "pictureBoxRecipe4";
            this.pictureBoxRecipe4.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe4.TabIndex = 0;
            this.pictureBoxRecipe4.TabStop = false;
            this.pictureBoxRecipe4.Click += new System.EventHandler(this.pictureBoxRecipe4_Click);
            // 
            // groupBoxRecipe3
            // 
            this.groupBoxRecipe3.Controls.Add(this.labelType3);
            this.groupBoxRecipe3.Controls.Add(this.label10);
            this.groupBoxRecipe3.Controls.Add(this.label11);
            this.groupBoxRecipe3.Controls.Add(this.labelName3);
            this.groupBoxRecipe3.Controls.Add(this.pictureBoxRecipe3);
            this.groupBoxRecipe3.Location = new System.Drawing.Point(398, 71);
            this.groupBoxRecipe3.Name = "groupBoxRecipe3";
            this.groupBoxRecipe3.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe3.TabIndex = 21;
            this.groupBoxRecipe3.TabStop = false;
            // 
            // labelType3
            // 
            this.labelType3.AutoSize = true;
            this.labelType3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType3.Location = new System.Drawing.Point(148, 152);
            this.labelType3.Name = "labelType3";
            this.labelType3.Size = new System.Drawing.Size(51, 20);
            this.labelType3.TabIndex = 4;
            this.labelType3.Text = "label1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(147, 112);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 24);
            this.label10.TabIndex = 3;
            this.label10.Text = "Type:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(147, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 24);
            this.label11.TabIndex = 2;
            this.label11.Text = "Name:";
            // 
            // labelName3
            // 
            this.labelName3.AutoSize = true;
            this.labelName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName3.Location = new System.Drawing.Point(148, 59);
            this.labelName3.Name = "labelName3";
            this.labelName3.Size = new System.Drawing.Size(51, 20);
            this.labelName3.TabIndex = 1;
            this.labelName3.Text = "label1";
            // 
            // pictureBoxRecipe3
            // 
            this.pictureBoxRecipe3.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe3.Name = "pictureBoxRecipe3";
            this.pictureBoxRecipe3.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe3.TabIndex = 0;
            this.pictureBoxRecipe3.TabStop = false;
            this.pictureBoxRecipe3.Click += new System.EventHandler(this.pictureBoxRecipe3_Click);
            // 
            // groupBoxRecipe2
            // 
            this.groupBoxRecipe2.Controls.Add(this.labelType2);
            this.groupBoxRecipe2.Controls.Add(this.label7);
            this.groupBoxRecipe2.Controls.Add(this.label8);
            this.groupBoxRecipe2.Controls.Add(this.labelName2);
            this.groupBoxRecipe2.Controls.Add(this.pictureBoxRecipe2);
            this.groupBoxRecipe2.Location = new System.Drawing.Point(12, 264);
            this.groupBoxRecipe2.Name = "groupBoxRecipe2";
            this.groupBoxRecipe2.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe2.TabIndex = 20;
            this.groupBoxRecipe2.TabStop = false;
            // 
            // labelType2
            // 
            this.labelType2.AutoSize = true;
            this.labelType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType2.Location = new System.Drawing.Point(148, 152);
            this.labelType2.Name = "labelType2";
            this.labelType2.Size = new System.Drawing.Size(51, 20);
            this.labelType2.TabIndex = 4;
            this.labelType2.Text = "label1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(147, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 24);
            this.label7.TabIndex = 3;
            this.label7.Text = "Type:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(147, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 24);
            this.label8.TabIndex = 2;
            this.label8.Text = "Name:";
            // 
            // labelName2
            // 
            this.labelName2.AutoSize = true;
            this.labelName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName2.Location = new System.Drawing.Point(148, 59);
            this.labelName2.Name = "labelName2";
            this.labelName2.Size = new System.Drawing.Size(51, 20);
            this.labelName2.TabIndex = 1;
            this.labelName2.Text = "label1";
            // 
            // pictureBoxRecipe2
            // 
            this.pictureBoxRecipe2.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe2.Name = "pictureBoxRecipe2";
            this.pictureBoxRecipe2.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe2.TabIndex = 0;
            this.pictureBoxRecipe2.TabStop = false;
            this.pictureBoxRecipe2.Click += new System.EventHandler(this.pictureBoxRecipe2_Click);
            // 
            // groupBoxRecipe1
            // 
            this.groupBoxRecipe1.Controls.Add(this.labelType1);
            this.groupBoxRecipe1.Controls.Add(this.label2);
            this.groupBoxRecipe1.Controls.Add(this.label1);
            this.groupBoxRecipe1.Controls.Add(this.labelName1);
            this.groupBoxRecipe1.Controls.Add(this.pictureBoxRecipe1);
            this.groupBoxRecipe1.Location = new System.Drawing.Point(12, 71);
            this.groupBoxRecipe1.Name = "groupBoxRecipe1";
            this.groupBoxRecipe1.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe1.TabIndex = 19;
            this.groupBoxRecipe1.TabStop = false;
            // 
            // labelType1
            // 
            this.labelType1.AutoSize = true;
            this.labelType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType1.Location = new System.Drawing.Point(148, 152);
            this.labelType1.Name = "labelType1";
            this.labelType1.Size = new System.Drawing.Size(51, 20);
            this.labelType1.TabIndex = 4;
            this.labelType1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(147, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // labelName1
            // 
            this.labelName1.AutoSize = true;
            this.labelName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName1.Location = new System.Drawing.Point(148, 59);
            this.labelName1.Name = "labelName1";
            this.labelName1.Size = new System.Drawing.Size(51, 20);
            this.labelName1.TabIndex = 1;
            this.labelName1.Text = "label1";
            // 
            // pictureBoxRecipe1
            // 
            this.pictureBoxRecipe1.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe1.Name = "pictureBoxRecipe1";
            this.pictureBoxRecipe1.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe1.TabIndex = 0;
            this.pictureBoxRecipe1.TabStop = false;
            this.pictureBoxRecipe1.Click += new System.EventHandler(this.pictureBoxRecipe1_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.Location = new System.Drawing.Point(687, 457);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(91, 37);
            this.buttonNext.TabIndex = 37;
            this.buttonNext.TabStop = false;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrevious.Location = new System.Drawing.Point(590, 457);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(91, 37);
            this.buttonPrevious.TabIndex = 36;
            this.buttonPrevious.TabStop = false;
            this.buttonPrevious.Text = "Previous";
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.buttonPrevious_Click);
            // 
            // pictureBoxAvatar
            // 
            this.pictureBoxAvatar.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAvatar.Image")));
            this.pictureBoxAvatar.Location = new System.Drawing.Point(720, 12);
            this.pictureBoxAvatar.Name = "pictureBoxAvatar";
            this.pictureBoxAvatar.Size = new System.Drawing.Size(58, 48);
            this.pictureBoxAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAvatar.TabIndex = 18;
            this.pictureBoxAvatar.TabStop = false;
            this.pictureBoxAvatar.Click += new System.EventHandler(this.pictureBoxAvatar_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 502);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrevious);
            this.Controls.Add(this.groupBoxRecipe4);
            this.Controls.Add(this.groupBoxRecipe3);
            this.Controls.Add(this.groupBoxRecipe2);
            this.Controls.Add(this.groupBoxRecipe1);
            this.Controls.Add(this.pictureBoxAvatar);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.buttonSearchRecipes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            this.groupBoxRecipe4.ResumeLayout(false);
            this.groupBoxRecipe4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).EndInit();
            this.groupBoxRecipe3.ResumeLayout(false);
            this.groupBoxRecipe3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).EndInit();
            this.groupBoxRecipe2.ResumeLayout(false);
            this.groupBoxRecipe2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).EndInit();
            this.groupBoxRecipe1.ResumeLayout(false);
            this.groupBoxRecipe1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAvatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxAvatar;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Button buttonSearchRecipes;
        private System.Windows.Forms.GroupBox groupBoxRecipe4;
        private System.Windows.Forms.Label labelType4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelName4;
        private System.Windows.Forms.PictureBox pictureBoxRecipe4;
        private System.Windows.Forms.GroupBox groupBoxRecipe3;
        private System.Windows.Forms.Label labelType3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelName3;
        private System.Windows.Forms.PictureBox pictureBoxRecipe3;
        private System.Windows.Forms.GroupBox groupBoxRecipe2;
        private System.Windows.Forms.Label labelType2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelName2;
        private System.Windows.Forms.PictureBox pictureBoxRecipe2;
        private System.Windows.Forms.GroupBox groupBoxRecipe1;
        private System.Windows.Forms.Label labelType1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelName1;
        private System.Windows.Forms.PictureBox pictureBoxRecipe1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
    }
}