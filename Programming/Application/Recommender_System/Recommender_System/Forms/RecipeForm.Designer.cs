﻿namespace Recommender_System.Forms
{
    partial class RecipeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxRecipe1 = new System.Windows.Forms.GroupBox();
            this.pictureBoxAlergic = new System.Windows.Forms.PictureBox();
            this.textBoxInstructions = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonRate1 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelType1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelName1 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.labelRecipeName1 = new System.Windows.Forms.Label();
            this.labelRecipeName2 = new System.Windows.Forms.Label();
            this.labelRecipeName3 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe1 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlergic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxRecipe1
            // 
            this.groupBoxRecipe1.Controls.Add(this.pictureBoxAlergic);
            this.groupBoxRecipe1.Controls.Add(this.textBoxInstructions);
            this.groupBoxRecipe1.Controls.Add(this.label4);
            this.groupBoxRecipe1.Controls.Add(this.buttonRate1);
            this.groupBoxRecipe1.Controls.Add(this.labelTimeToPrepare1);
            this.groupBoxRecipe1.Controls.Add(this.label3);
            this.groupBoxRecipe1.Controls.Add(this.labelType1);
            this.groupBoxRecipe1.Controls.Add(this.label2);
            this.groupBoxRecipe1.Controls.Add(this.label1);
            this.groupBoxRecipe1.Controls.Add(this.labelName1);
            this.groupBoxRecipe1.Controls.Add(this.pictureBoxRecipe);
            this.groupBoxRecipe1.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRecipe1.Name = "groupBoxRecipe1";
            this.groupBoxRecipe1.Size = new System.Drawing.Size(787, 300);
            this.groupBoxRecipe1.TabIndex = 1;
            this.groupBoxRecipe1.TabStop = false;
            // 
            // pictureBoxAlergic
            // 
            this.pictureBoxAlergic.Image = global::Recommender_System.Properties.Resources.warning;
            this.pictureBoxAlergic.Location = new System.Drawing.Point(694, 13);
            this.pictureBoxAlergic.Name = "pictureBoxAlergic";
            this.pictureBoxAlergic.Size = new System.Drawing.Size(86, 82);
            this.pictureBoxAlergic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAlergic.TabIndex = 9;
            this.pictureBoxAlergic.TabStop = false;
            this.pictureBoxAlergic.Visible = false;
            // 
            // textBoxInstructions
            // 
            this.textBoxInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInstructions.Location = new System.Drawing.Point(394, 125);
            this.textBoxInstructions.Multiline = true;
            this.textBoxInstructions.Name = "textBoxInstructions";
            this.textBoxInstructions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxInstructions.Size = new System.Drawing.Size(387, 127);
            this.textBoxInstructions.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(391, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Instructions:";
            // 
            // buttonRate1
            // 
            this.buttonRate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate1.Location = new System.Drawing.Point(695, 258);
            this.buttonRate1.Name = "buttonRate1";
            this.buttonRate1.Size = new System.Drawing.Size(86, 36);
            this.buttonRate1.TabIndex = 7;
            this.buttonRate1.Text = "RATE";
            this.buttonRate1.UseVisualStyleBackColor = true;
            this.buttonRate1.Click += new System.EventHandler(this.buttonRate1_Click);
            // 
            // labelTimeToPrepare1
            // 
            this.labelTimeToPrepare1.AutoSize = true;
            this.labelTimeToPrepare1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare1.Location = new System.Drawing.Point(526, 79);
            this.labelTimeToPrepare1.Name = "labelTimeToPrepare1";
            this.labelTimeToPrepare1.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare1.TabIndex = 6;
            this.labelTimeToPrepare1.Text = "label1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(391, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Time to prepare:";
            // 
            // labelType1
            // 
            this.labelType1.AutoSize = true;
            this.labelType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType1.Location = new System.Drawing.Point(450, 47);
            this.labelType1.Name = "labelType1";
            this.labelType1.Size = new System.Drawing.Size(45, 16);
            this.labelType1.TabIndex = 4;
            this.labelType1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(391, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(391, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // labelName1
            // 
            this.labelName1.AutoSize = true;
            this.labelName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName1.Location = new System.Drawing.Point(450, 16);
            this.labelName1.Name = "labelName1";
            this.labelName1.Size = new System.Drawing.Size(45, 16);
            this.labelName1.TabIndex = 1;
            this.labelName1.Text = "label1";
            // 
            // pictureBoxRecipe
            // 
            this.pictureBoxRecipe.Location = new System.Drawing.Point(6, 10);
            this.pictureBoxRecipe.Name = "pictureBoxRecipe";
            this.pictureBoxRecipe.Size = new System.Drawing.Size(379, 284);
            this.pictureBoxRecipe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe.TabIndex = 0;
            this.pictureBoxRecipe.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 327);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(336, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "Top Recommendations for You";
            // 
            // labelRecipeName1
            // 
            this.labelRecipeName1.AutoSize = true;
            this.labelRecipeName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecipeName1.Location = new System.Drawing.Point(14, 378);
            this.labelRecipeName1.Name = "labelRecipeName1";
            this.labelRecipeName1.Size = new System.Drawing.Size(60, 24);
            this.labelRecipeName1.TabIndex = 6;
            this.labelRecipeName1.Text = "label1";
            // 
            // labelRecipeName2
            // 
            this.labelRecipeName2.AutoSize = true;
            this.labelRecipeName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecipeName2.Location = new System.Drawing.Point(231, 378);
            this.labelRecipeName2.Name = "labelRecipeName2";
            this.labelRecipeName2.Size = new System.Drawing.Size(60, 24);
            this.labelRecipeName2.TabIndex = 7;
            this.labelRecipeName2.Text = "label1";
            // 
            // labelRecipeName3
            // 
            this.labelRecipeName3.AutoSize = true;
            this.labelRecipeName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecipeName3.Location = new System.Drawing.Point(461, 378);
            this.labelRecipeName3.Name = "labelRecipeName3";
            this.labelRecipeName3.Size = new System.Drawing.Size(60, 24);
            this.labelRecipeName3.TabIndex = 8;
            this.labelRecipeName3.Text = "label1";
            // 
            // pictureBoxRecipe3
            // 
            this.pictureBoxRecipe3.Location = new System.Drawing.Point(465, 414);
            this.pictureBoxRecipe3.Name = "pictureBoxRecipe3";
            this.pictureBoxRecipe3.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe3.TabIndex = 2;
            this.pictureBoxRecipe3.TabStop = false;
            this.pictureBoxRecipe3.Click += new System.EventHandler(this.pictureBoxRecipe3_Click);
            // 
            // pictureBoxRecipe2
            // 
            this.pictureBoxRecipe2.Location = new System.Drawing.Point(235, 414);
            this.pictureBoxRecipe2.Name = "pictureBoxRecipe2";
            this.pictureBoxRecipe2.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe2.TabIndex = 3;
            this.pictureBoxRecipe2.TabStop = false;
            this.pictureBoxRecipe2.Click += new System.EventHandler(this.pictureBoxRecipe2_Click);
            // 
            // pictureBoxRecipe1
            // 
            this.pictureBoxRecipe1.Location = new System.Drawing.Point(18, 414);
            this.pictureBoxRecipe1.Name = "pictureBoxRecipe1";
            this.pictureBoxRecipe1.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe1.TabIndex = 4;
            this.pictureBoxRecipe1.TabStop = false;
            this.pictureBoxRecipe1.Click += new System.EventHandler(this.pictureBoxRecipe1_Click);
            // 
            // RecipeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 579);
            this.Controls.Add(this.labelRecipeName3);
            this.Controls.Add(this.labelRecipeName2);
            this.Controls.Add(this.labelRecipeName1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBoxRecipe3);
            this.Controls.Add(this.pictureBoxRecipe2);
            this.Controls.Add(this.pictureBoxRecipe1);
            this.Controls.Add(this.groupBoxRecipe1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "RecipeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            this.groupBoxRecipe1.ResumeLayout(false);
            this.groupBoxRecipe1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlergic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxRecipe1;
        private System.Windows.Forms.TextBox textBoxInstructions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonRate1;
        private System.Windows.Forms.Label labelTimeToPrepare1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelType1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelName1;
        private System.Windows.Forms.PictureBox pictureBoxRecipe;
        private System.Windows.Forms.PictureBox pictureBoxRecipe3;
        private System.Windows.Forms.PictureBox pictureBoxRecipe2;
        private System.Windows.Forms.PictureBox pictureBoxRecipe1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelRecipeName1;
        private System.Windows.Forms.Label labelRecipeName2;
        private System.Windows.Forms.Label labelRecipeName3;
        private System.Windows.Forms.PictureBox pictureBoxAlergic;
    }
}