﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class EditProfileForm : Form
    {
        GraphClient client;
        public User user;

        public EditProfileForm(GraphClient client, User user)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;

            textBoxUsername.Text = user.username;
            textBoxFirstName.Text = user.firstName;
            textBoxLastName.Text = user.lastName;
            textBoxPassword.Text = user.password;
            textBoxRetypePassword.Text = user.password;
            textBoxAge.Text = user.age.ToString();
            textBoxCountry.Text = user.country;
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            int age;
            string name = textBoxFirstName.Text;
            string surname = textBoxLastName.Text;
            string password = textBoxPassword.Text;
            string retype = textBoxRetypePassword.Text;
            bool succeed = Int32.TryParse(textBoxAge.Text, out age);
            string country = textBoxCountry.Text;
            if (String.IsNullOrWhiteSpace(name))
                MessageBox.Show("First name field is empty! Please try again.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (String.IsNullOrWhiteSpace(surname))
                MessageBox.Show("Last name field is empty! Please try again.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (!succeed)
                MessageBox.Show("Incorrect age field! Please try again.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (String.IsNullOrWhiteSpace(country))
                MessageBox.Show("Country field is empty! Please try again.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (password != retype)
                MessageBox.Show("Passwords do not match! Please try again.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (!String.IsNullOrWhiteSpace(password))
                    user.password = password;
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("id_username", user.username);
                dictionary.Add("id_name", name);
                dictionary.Add("id_surname", surname);
                dictionary.Add("id_password", user.password);
                dictionary.Add("id_age", age);
                dictionary.Add("id_country", country);
                var query = new CypherQuery("MATCH (n:User) WHERE n.username = {id_username} SET n.firstName = {id_name}, n.lastName = {id_surname}, n.password = {id_password}, n.age = {id_age}, n.country = {id_country} RETURN n", dictionary, CypherResultMode.Set);
                user = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).FirstOrDefault();
                Close();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}