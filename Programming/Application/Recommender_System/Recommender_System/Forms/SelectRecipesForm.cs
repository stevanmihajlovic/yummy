﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class SelectRecipesForm : Form
    {
        GraphClient client;
        User user;
        List<Recipe> recipes = new List<Recipe>();
        List<bool> rated = new List<bool>();

        public SelectRecipesForm(GraphClient client, User user)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;

            for (int i = 0; i < 6; i++)
                rated.Add(false);
            
            loadRandomRecipes();

            pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[0].name.Replace(' ', '_').ToLower());
            labelName1.Text = recipes[0].name;
            labelType1.Text = recipes[0].type;
            labelTimeToPrepare1.Text = recipes[0].timeToPrepare;

            pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[1].name.Replace(' ', '_').ToLower());
            labelName2.Text = recipes[1].name;
            labelType2.Text = recipes[1].type;
            labelTimeToPrepare2.Text = recipes[1].timeToPrepare;

            pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[2].name.Replace(' ', '_').ToLower());
            labelName3.Text = recipes[2].name;
            labelType3.Text = recipes[2].type;
            labelTimeToPrepare3.Text = recipes[2].timeToPrepare;

            pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[3].name.Replace(' ', '_').ToLower());
            labelName4.Text = recipes[3].name;
            labelType4.Text = recipes[3].type;
            labelTimeToPrepare4.Text = recipes[3].timeToPrepare;

            pictureBoxRecipe5.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[4].name.Replace(' ', '_').ToLower());
            labelName5.Text = recipes[4].name;
            labelType5.Text = recipes[4].type;
            labelTimeToPrepare5.Text = recipes[4].timeToPrepare;

            pictureBoxRecipe6.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[5].name.Replace(' ', '_').ToLower());
            labelName6.Text = recipes[5].name;
            labelType6.Text = recipes[5].type;
            labelTimeToPrepare6.Text = recipes[5].timeToPrepare;
        }

        private void loadRandomRecipes()
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("id", user.username);
            var query = new CypherQuery("MATCH (n:Recipe), (m:User), (i:Ingredient) WHERE m.username = {id} AND NOT(i-[:partOf]->n) AND m-[:alergic]->i return n", dictionary, CypherResultMode.Set);
            List<Recipe> temp_recipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();
            if (temp_recipes.Count == 0)
            {
                query = new CypherQuery("MATCH (n:Recipe) RETURN n", new Dictionary<string, object>(), CypherResultMode.Set);
                temp_recipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();
            }

            List<int> temp_indices = new List<int>();
            Random random = new Random();
            while (temp_indices.Count < 6)
            {
                int number = random.Next(temp_recipes.Count);
                if (!temp_indices.Contains(number))
                {
                    temp_indices.Add(number);
                    recipes.Add(temp_recipes[number]);
                }
            }
        }

        private void buttonRate1_Click(object sender, EventArgs e)
        {
            rated[0] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[0]);
            form.ShowDialog();
        }

        private void buttonRate2_Click(object sender, EventArgs e)
        {
            rated[1] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[1]);
            form.ShowDialog();
        }

        private void buttonRate3_Click(object sender, EventArgs e)
        {
            rated[2] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[2]);
            form.ShowDialog();
        }

        private void buttonRate4_Click(object sender, EventArgs e)
        {
            rated[3] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[3]);
            form.ShowDialog();
        }

        private void buttonRate5_Click(object sender, EventArgs e)
        {
            rated[4] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[4]);
            form.ShowDialog();
        }

        private void buttonRate6_Click(object sender, EventArgs e)
        {
            rated[5] = true;
            RateRecipeForm form = new RateRecipeForm(client, user, recipes[5]);
            form.ShowDialog();
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            if (rated.Contains(false))
                MessageBox.Show("You have to rate all recipes", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                Hide();
                MainForm form = new MainForm(client, user);
                form.ShowDialog();
                Close();
            }
        }
    }
}