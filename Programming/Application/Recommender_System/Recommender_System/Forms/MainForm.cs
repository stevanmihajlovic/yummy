﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class MainForm : Form
    {
        GraphClient client;
        User user;
        List<Recipe> recipes;
        int index;

        public MainForm(GraphClient client, User user)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;

            labelUser.Text += user.username;
            buttonPrevious.Visible = false;

            var query = new CypherQuery("MATCH (n:Recipe) RETURN n", new Dictionary<string, object>(), CypherResultMode.Set);
            recipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();

            index = 0;
            if (index < recipes.Count)
            {
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelType1.Text = recipes[index].type;
                index++;
            }
            else
            {
                groupBoxRecipe1.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelType2.Text = recipes[index].type;
                index++;
            }
            else
            {
                groupBoxRecipe2.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelType3.Text = recipes[index].type;
                index++;
            }
            else
            {
                groupBoxRecipe3.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelType4.Text = recipes[index].type;
                index++;
            }
            else
            {
                groupBoxRecipe4.Visible = false;
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (groupBoxRecipe1.Visible == false || groupBoxRecipe2.Visible == false || groupBoxRecipe3.Visible == false || groupBoxRecipe4.Visible == false || recipes.Count == 4)
                return;
            if (index < recipes.Count)
            {
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelType1.Text = recipes[index].type;
                buttonPrevious.Visible = true;
                index++;
            }
            else
                return;

            if (index < recipes.Count)
            {
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelType2.Text = recipes[index].type;
                index++;
            }
            else
            {
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
                groupBoxRecipe2.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelType3.Text = recipes[index].type;
                index++;
            }
            else
            {
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
                groupBoxRecipe3.Visible = false;
            }

            if (index < recipes.Count)
            {
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelType4.Text = recipes[index].type;
                index++;
            }
            else
            {
                buttonPrevious.Visible = true;
                buttonNext.Visible = false;
                groupBoxRecipe4.Visible = false;
            }
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            buttonNext.Visible = true;
            if (recipes.Count <= 4)
                return;
            while (index % 4 != 0)
                index++;
            if (index > 4)
                index -= 4;

            if (index > 0)
            {
                index--;
                pictureBoxRecipe4.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName4.Text = recipes[index].name;
                labelType4.Text = recipes[index].type;
                groupBoxRecipe4.Visible = true;
            }
            else
            {
                groupBoxRecipe4.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName3.Text = recipes[index].name;
                labelType3.Text = recipes[index].type;
                groupBoxRecipe3.Visible = true;
            }
            else
            {
                groupBoxRecipe3.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName2.Text = recipes[index].name;
                labelType2.Text = recipes[index].type;
                groupBoxRecipe2.Visible = true;
            }
            else
            {
                groupBoxRecipe2.Visible = false;
            }

            if (index > 0)
            {
                index--;
                pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipes[index].name.Replace(' ', '_').ToLower());
                labelName1.Text = recipes[index].name;
                labelType1.Text = recipes[index].type;
                groupBoxRecipe1.Visible = true;
            }
            else
            {
                groupBoxRecipe1.Visible = false;
            }

            index += 4;
            if (index == 4)
                buttonPrevious.Visible = false;
        }

        private void buttonSearchRecipes_Click(object sender, EventArgs e)
        {
            Hide();
            SearchForm form = new SearchForm(client, user);
            form.ShowDialog();
            Show();
        }

        private void pictureBoxRecipe1_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, recipes[(index - 1)/ 4 * 4]);
            form.ShowDialog();
            Show();
        }

        private void pictureBoxRecipe2_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, recipes[(index - 1) / 4 * 4 + 1]);
            form.ShowDialog();
            Show();
        }

        private void pictureBoxRecipe3_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, recipes[(index - 1) / 4 * 4  + 2]);
            form.ShowDialog();
            Show();
        }

        private void pictureBoxRecipe4_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, recipes[(index - 1) / 4 * 4 + 3]);
            form.ShowDialog();
            Show();
        }

        private void pictureBoxAvatar_Click(object sender, EventArgs e)
        {
            Hide();
            MyProfileForm form = new MyProfileForm(client, user);
            form.ShowDialog();
            Show();
        }
    }
}