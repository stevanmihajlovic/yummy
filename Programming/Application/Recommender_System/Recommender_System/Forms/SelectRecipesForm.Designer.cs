﻿namespace Recommender_System.Forms
{
    partial class SelectRecipesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxRecipe1 = new System.Windows.Forms.GroupBox();
            this.buttonRate1 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelType1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelName1 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe1 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe2 = new System.Windows.Forms.GroupBox();
            this.buttonRate2 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelType2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelName2 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe2 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe3 = new System.Windows.Forms.GroupBox();
            this.buttonRate3 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelType3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelName3 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe3 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe4 = new System.Windows.Forms.GroupBox();
            this.buttonRate4 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelType4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelName4 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe4 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe5 = new System.Windows.Forms.GroupBox();
            this.buttonRate5 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelType5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelName5 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe5 = new System.Windows.Forms.PictureBox();
            this.groupBoxRecipe6 = new System.Windows.Forms.GroupBox();
            this.buttonRate6 = new System.Windows.Forms.Button();
            this.labelTimeToPrepare6 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelType6 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelName6 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe6 = new System.Windows.Forms.PictureBox();
            this.buttonDone = new System.Windows.Forms.Button();
            this.groupBoxRecipe1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).BeginInit();
            this.groupBoxRecipe2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).BeginInit();
            this.groupBoxRecipe3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).BeginInit();
            this.groupBoxRecipe4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).BeginInit();
            this.groupBoxRecipe5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe5)).BeginInit();
            this.groupBoxRecipe6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe6)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxRecipe1
            // 
            this.groupBoxRecipe1.Controls.Add(this.buttonRate1);
            this.groupBoxRecipe1.Controls.Add(this.labelTimeToPrepare1);
            this.groupBoxRecipe1.Controls.Add(this.label3);
            this.groupBoxRecipe1.Controls.Add(this.labelType1);
            this.groupBoxRecipe1.Controls.Add(this.label2);
            this.groupBoxRecipe1.Controls.Add(this.label1);
            this.groupBoxRecipe1.Controls.Add(this.labelName1);
            this.groupBoxRecipe1.Controls.Add(this.pictureBoxRecipe1);
            this.groupBoxRecipe1.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRecipe1.Name = "groupBoxRecipe1";
            this.groupBoxRecipe1.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe1.TabIndex = 0;
            this.groupBoxRecipe1.TabStop = false;
            // 
            // buttonRate1
            // 
            this.buttonRate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate1.Location = new System.Drawing.Point(150, 136);
            this.buttonRate1.Name = "buttonRate1";
            this.buttonRate1.Size = new System.Drawing.Size(86, 36);
            this.buttonRate1.TabIndex = 7;
            this.buttonRate1.Text = "RATE";
            this.buttonRate1.UseVisualStyleBackColor = true;
            this.buttonRate1.Click += new System.EventHandler(this.buttonRate1_Click);
            // 
            // labelTimeToPrepare1
            // 
            this.labelTimeToPrepare1.AutoSize = true;
            this.labelTimeToPrepare1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare1.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare1.Name = "labelTimeToPrepare1";
            this.labelTimeToPrepare1.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare1.TabIndex = 6;
            this.labelTimeToPrepare1.Text = "label1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Time to prepare:";
            // 
            // labelType1
            // 
            this.labelType1.AutoSize = true;
            this.labelType1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType1.Location = new System.Drawing.Point(206, 63);
            this.labelType1.Name = "labelType1";
            this.labelType1.Size = new System.Drawing.Size(45, 16);
            this.labelType1.TabIndex = 4;
            this.labelType1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(147, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // labelName1
            // 
            this.labelName1.AutoSize = true;
            this.labelName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName1.Location = new System.Drawing.Point(206, 19);
            this.labelName1.Name = "labelName1";
            this.labelName1.Size = new System.Drawing.Size(45, 16);
            this.labelName1.TabIndex = 1;
            this.labelName1.Text = "label1";
            // 
            // pictureBoxRecipe1
            // 
            this.pictureBoxRecipe1.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe1.Name = "pictureBoxRecipe1";
            this.pictureBoxRecipe1.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe1.TabIndex = 0;
            this.pictureBoxRecipe1.TabStop = false;
            // 
            // groupBoxRecipe2
            // 
            this.groupBoxRecipe2.Controls.Add(this.buttonRate2);
            this.groupBoxRecipe2.Controls.Add(this.labelTimeToPrepare2);
            this.groupBoxRecipe2.Controls.Add(this.label5);
            this.groupBoxRecipe2.Controls.Add(this.labelType2);
            this.groupBoxRecipe2.Controls.Add(this.label7);
            this.groupBoxRecipe2.Controls.Add(this.label8);
            this.groupBoxRecipe2.Controls.Add(this.labelName2);
            this.groupBoxRecipe2.Controls.Add(this.pictureBoxRecipe2);
            this.groupBoxRecipe2.Location = new System.Drawing.Point(12, 205);
            this.groupBoxRecipe2.Name = "groupBoxRecipe2";
            this.groupBoxRecipe2.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe2.TabIndex = 1;
            this.groupBoxRecipe2.TabStop = false;
            // 
            // buttonRate2
            // 
            this.buttonRate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate2.Location = new System.Drawing.Point(150, 136);
            this.buttonRate2.Name = "buttonRate2";
            this.buttonRate2.Size = new System.Drawing.Size(86, 36);
            this.buttonRate2.TabIndex = 8;
            this.buttonRate2.Text = "RATE";
            this.buttonRate2.UseVisualStyleBackColor = true;
            this.buttonRate2.Click += new System.EventHandler(this.buttonRate2_Click);
            // 
            // labelTimeToPrepare2
            // 
            this.labelTimeToPrepare2.AutoSize = true;
            this.labelTimeToPrepare2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare2.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare2.Name = "labelTimeToPrepare2";
            this.labelTimeToPrepare2.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare2.TabIndex = 6;
            this.labelTimeToPrepare2.Text = "label1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(147, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Time to prepare:";
            // 
            // labelType2
            // 
            this.labelType2.AutoSize = true;
            this.labelType2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType2.Location = new System.Drawing.Point(206, 63);
            this.labelType2.Name = "labelType2";
            this.labelType2.Size = new System.Drawing.Size(45, 16);
            this.labelType2.TabIndex = 4;
            this.labelType2.Text = "label1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(147, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 3;
            this.label7.Text = "Type:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(147, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Name:";
            // 
            // labelName2
            // 
            this.labelName2.AutoSize = true;
            this.labelName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName2.Location = new System.Drawing.Point(206, 19);
            this.labelName2.Name = "labelName2";
            this.labelName2.Size = new System.Drawing.Size(45, 16);
            this.labelName2.TabIndex = 1;
            this.labelName2.Text = "label1";
            // 
            // pictureBoxRecipe2
            // 
            this.pictureBoxRecipe2.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe2.Name = "pictureBoxRecipe2";
            this.pictureBoxRecipe2.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe2.TabIndex = 0;
            this.pictureBoxRecipe2.TabStop = false;
            // 
            // groupBoxRecipe3
            // 
            this.groupBoxRecipe3.Controls.Add(this.buttonRate3);
            this.groupBoxRecipe3.Controls.Add(this.labelTimeToPrepare3);
            this.groupBoxRecipe3.Controls.Add(this.label6);
            this.groupBoxRecipe3.Controls.Add(this.labelType3);
            this.groupBoxRecipe3.Controls.Add(this.label10);
            this.groupBoxRecipe3.Controls.Add(this.label11);
            this.groupBoxRecipe3.Controls.Add(this.labelName3);
            this.groupBoxRecipe3.Controls.Add(this.pictureBoxRecipe3);
            this.groupBoxRecipe3.Location = new System.Drawing.Point(398, 12);
            this.groupBoxRecipe3.Name = "groupBoxRecipe3";
            this.groupBoxRecipe3.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe3.TabIndex = 2;
            this.groupBoxRecipe3.TabStop = false;
            // 
            // buttonRate3
            // 
            this.buttonRate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate3.Location = new System.Drawing.Point(150, 136);
            this.buttonRate3.Name = "buttonRate3";
            this.buttonRate3.Size = new System.Drawing.Size(86, 36);
            this.buttonRate3.TabIndex = 8;
            this.buttonRate3.Text = "RATE";
            this.buttonRate3.UseVisualStyleBackColor = true;
            this.buttonRate3.Click += new System.EventHandler(this.buttonRate3_Click);
            // 
            // labelTimeToPrepare3
            // 
            this.labelTimeToPrepare3.AutoSize = true;
            this.labelTimeToPrepare3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare3.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare3.Name = "labelTimeToPrepare3";
            this.labelTimeToPrepare3.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare3.TabIndex = 6;
            this.labelTimeToPrepare3.Text = "label1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(147, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Time to prepare:";
            // 
            // labelType3
            // 
            this.labelType3.AutoSize = true;
            this.labelType3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType3.Location = new System.Drawing.Point(206, 63);
            this.labelType3.Name = "labelType3";
            this.labelType3.Size = new System.Drawing.Size(45, 16);
            this.labelType3.TabIndex = 4;
            this.labelType3.Text = "label1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(147, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Type:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(147, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Name:";
            // 
            // labelName3
            // 
            this.labelName3.AutoSize = true;
            this.labelName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName3.Location = new System.Drawing.Point(206, 19);
            this.labelName3.Name = "labelName3";
            this.labelName3.Size = new System.Drawing.Size(45, 16);
            this.labelName3.TabIndex = 1;
            this.labelName3.Text = "label1";
            // 
            // pictureBoxRecipe3
            // 
            this.pictureBoxRecipe3.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe3.Name = "pictureBoxRecipe3";
            this.pictureBoxRecipe3.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe3.TabIndex = 0;
            this.pictureBoxRecipe3.TabStop = false;
            // 
            // groupBoxRecipe4
            // 
            this.groupBoxRecipe4.Controls.Add(this.buttonRate4);
            this.groupBoxRecipe4.Controls.Add(this.labelTimeToPrepare4);
            this.groupBoxRecipe4.Controls.Add(this.label14);
            this.groupBoxRecipe4.Controls.Add(this.labelType4);
            this.groupBoxRecipe4.Controls.Add(this.label16);
            this.groupBoxRecipe4.Controls.Add(this.label17);
            this.groupBoxRecipe4.Controls.Add(this.labelName4);
            this.groupBoxRecipe4.Controls.Add(this.pictureBoxRecipe4);
            this.groupBoxRecipe4.Location = new System.Drawing.Point(398, 205);
            this.groupBoxRecipe4.Name = "groupBoxRecipe4";
            this.groupBoxRecipe4.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe4.TabIndex = 3;
            this.groupBoxRecipe4.TabStop = false;
            // 
            // buttonRate4
            // 
            this.buttonRate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate4.Location = new System.Drawing.Point(150, 136);
            this.buttonRate4.Name = "buttonRate4";
            this.buttonRate4.Size = new System.Drawing.Size(86, 36);
            this.buttonRate4.TabIndex = 8;
            this.buttonRate4.Text = "RATE";
            this.buttonRate4.UseVisualStyleBackColor = true;
            this.buttonRate4.Click += new System.EventHandler(this.buttonRate4_Click);
            // 
            // labelTimeToPrepare4
            // 
            this.labelTimeToPrepare4.AutoSize = true;
            this.labelTimeToPrepare4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare4.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare4.Name = "labelTimeToPrepare4";
            this.labelTimeToPrepare4.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare4.TabIndex = 6;
            this.labelTimeToPrepare4.Text = "label1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(147, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 16);
            this.label14.TabIndex = 5;
            this.label14.Text = "Time to prepare:";
            // 
            // labelType4
            // 
            this.labelType4.AutoSize = true;
            this.labelType4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType4.Location = new System.Drawing.Point(206, 63);
            this.labelType4.Name = "labelType4";
            this.labelType4.Size = new System.Drawing.Size(45, 16);
            this.labelType4.TabIndex = 4;
            this.labelType4.Text = "label1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(147, 63);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 16);
            this.label16.TabIndex = 3;
            this.label16.Text = "Type:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(147, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 16);
            this.label17.TabIndex = 2;
            this.label17.Text = "Name:";
            // 
            // labelName4
            // 
            this.labelName4.AutoSize = true;
            this.labelName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName4.Location = new System.Drawing.Point(206, 19);
            this.labelName4.Name = "labelName4";
            this.labelName4.Size = new System.Drawing.Size(45, 16);
            this.labelName4.TabIndex = 1;
            this.labelName4.Text = "label1";
            // 
            // pictureBoxRecipe4
            // 
            this.pictureBoxRecipe4.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe4.Name = "pictureBoxRecipe4";
            this.pictureBoxRecipe4.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe4.TabIndex = 0;
            this.pictureBoxRecipe4.TabStop = false;
            // 
            // groupBoxRecipe5
            // 
            this.groupBoxRecipe5.Controls.Add(this.buttonRate5);
            this.groupBoxRecipe5.Controls.Add(this.labelTimeToPrepare5);
            this.groupBoxRecipe5.Controls.Add(this.label9);
            this.groupBoxRecipe5.Controls.Add(this.labelType5);
            this.groupBoxRecipe5.Controls.Add(this.label13);
            this.groupBoxRecipe5.Controls.Add(this.label15);
            this.groupBoxRecipe5.Controls.Add(this.labelName5);
            this.groupBoxRecipe5.Controls.Add(this.pictureBoxRecipe5);
            this.groupBoxRecipe5.Location = new System.Drawing.Point(784, 12);
            this.groupBoxRecipe5.Name = "groupBoxRecipe5";
            this.groupBoxRecipe5.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe5.TabIndex = 4;
            this.groupBoxRecipe5.TabStop = false;
            // 
            // buttonRate5
            // 
            this.buttonRate5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate5.Location = new System.Drawing.Point(150, 136);
            this.buttonRate5.Name = "buttonRate5";
            this.buttonRate5.Size = new System.Drawing.Size(86, 36);
            this.buttonRate5.TabIndex = 8;
            this.buttonRate5.Text = "RATE";
            this.buttonRate5.UseVisualStyleBackColor = true;
            this.buttonRate5.Click += new System.EventHandler(this.buttonRate5_Click);
            // 
            // labelTimeToPrepare5
            // 
            this.labelTimeToPrepare5.AutoSize = true;
            this.labelTimeToPrepare5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare5.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare5.Name = "labelTimeToPrepare5";
            this.labelTimeToPrepare5.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare5.TabIndex = 6;
            this.labelTimeToPrepare5.Text = "label1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(147, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 16);
            this.label9.TabIndex = 5;
            this.label9.Text = "Time to prepare:";
            // 
            // labelType5
            // 
            this.labelType5.AutoSize = true;
            this.labelType5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType5.Location = new System.Drawing.Point(206, 63);
            this.labelType5.Name = "labelType5";
            this.labelType5.Size = new System.Drawing.Size(45, 16);
            this.labelType5.TabIndex = 4;
            this.labelType5.Text = "label1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(147, 63);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 16);
            this.label13.TabIndex = 3;
            this.label13.Text = "Type:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(147, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "Name:";
            // 
            // labelName5
            // 
            this.labelName5.AutoSize = true;
            this.labelName5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName5.Location = new System.Drawing.Point(206, 19);
            this.labelName5.Name = "labelName5";
            this.labelName5.Size = new System.Drawing.Size(45, 16);
            this.labelName5.TabIndex = 1;
            this.labelName5.Text = "label1";
            // 
            // pictureBoxRecipe5
            // 
            this.pictureBoxRecipe5.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe5.Name = "pictureBoxRecipe5";
            this.pictureBoxRecipe5.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe5.TabIndex = 0;
            this.pictureBoxRecipe5.TabStop = false;
            // 
            // groupBoxRecipe6
            // 
            this.groupBoxRecipe6.Controls.Add(this.buttonRate6);
            this.groupBoxRecipe6.Controls.Add(this.labelTimeToPrepare6);
            this.groupBoxRecipe6.Controls.Add(this.label20);
            this.groupBoxRecipe6.Controls.Add(this.labelType6);
            this.groupBoxRecipe6.Controls.Add(this.label22);
            this.groupBoxRecipe6.Controls.Add(this.label23);
            this.groupBoxRecipe6.Controls.Add(this.labelName6);
            this.groupBoxRecipe6.Controls.Add(this.pictureBoxRecipe6);
            this.groupBoxRecipe6.Location = new System.Drawing.Point(784, 205);
            this.groupBoxRecipe6.Name = "groupBoxRecipe6";
            this.groupBoxRecipe6.Size = new System.Drawing.Size(380, 187);
            this.groupBoxRecipe6.TabIndex = 5;
            this.groupBoxRecipe6.TabStop = false;
            // 
            // buttonRate6
            // 
            this.buttonRate6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRate6.Location = new System.Drawing.Point(150, 136);
            this.buttonRate6.Name = "buttonRate6";
            this.buttonRate6.Size = new System.Drawing.Size(86, 36);
            this.buttonRate6.TabIndex = 8;
            this.buttonRate6.Text = "RATE";
            this.buttonRate6.UseVisualStyleBackColor = true;
            this.buttonRate6.Click += new System.EventHandler(this.buttonRate6_Click);
            // 
            // labelTimeToPrepare6
            // 
            this.labelTimeToPrepare6.AutoSize = true;
            this.labelTimeToPrepare6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeToPrepare6.Location = new System.Drawing.Point(282, 106);
            this.labelTimeToPrepare6.Name = "labelTimeToPrepare6";
            this.labelTimeToPrepare6.Size = new System.Drawing.Size(45, 16);
            this.labelTimeToPrepare6.TabIndex = 6;
            this.labelTimeToPrepare6.Text = "label1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(147, 106);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 16);
            this.label20.TabIndex = 5;
            this.label20.Text = "Time to prepare:";
            // 
            // labelType6
            // 
            this.labelType6.AutoSize = true;
            this.labelType6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType6.Location = new System.Drawing.Point(206, 63);
            this.labelType6.Name = "labelType6";
            this.labelType6.Size = new System.Drawing.Size(45, 16);
            this.labelType6.TabIndex = 4;
            this.labelType6.Text = "label1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(147, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 16);
            this.label22.TabIndex = 3;
            this.label22.Text = "Type:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(147, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 16);
            this.label23.TabIndex = 2;
            this.label23.Text = "Name:";
            // 
            // labelName6
            // 
            this.labelName6.AutoSize = true;
            this.labelName6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName6.Location = new System.Drawing.Point(206, 19);
            this.labelName6.Name = "labelName6";
            this.labelName6.Size = new System.Drawing.Size(45, 16);
            this.labelName6.TabIndex = 1;
            this.labelName6.Text = "label1";
            // 
            // pictureBoxRecipe6
            // 
            this.pictureBoxRecipe6.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxRecipe6.Name = "pictureBoxRecipe6";
            this.pictureBoxRecipe6.Size = new System.Drawing.Size(135, 153);
            this.pictureBoxRecipe6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe6.TabIndex = 0;
            this.pictureBoxRecipe6.TabStop = false;
            // 
            // buttonDone
            // 
            this.buttonDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDone.Location = new System.Drawing.Point(1026, 398);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(138, 53);
            this.buttonDone.TabIndex = 6;
            this.buttonDone.TabStop = false;
            this.buttonDone.Text = "DONE";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // SelectRecipesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 463);
            this.Controls.Add(this.buttonDone);
            this.Controls.Add(this.groupBoxRecipe6);
            this.Controls.Add(this.groupBoxRecipe5);
            this.Controls.Add(this.groupBoxRecipe4);
            this.Controls.Add(this.groupBoxRecipe3);
            this.Controls.Add(this.groupBoxRecipe2);
            this.Controls.Add(this.groupBoxRecipe1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SelectRecipesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            this.groupBoxRecipe1.ResumeLayout(false);
            this.groupBoxRecipe1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).EndInit();
            this.groupBoxRecipe2.ResumeLayout(false);
            this.groupBoxRecipe2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).EndInit();
            this.groupBoxRecipe3.ResumeLayout(false);
            this.groupBoxRecipe3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).EndInit();
            this.groupBoxRecipe4.ResumeLayout(false);
            this.groupBoxRecipe4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).EndInit();
            this.groupBoxRecipe5.ResumeLayout(false);
            this.groupBoxRecipe5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe5)).EndInit();
            this.groupBoxRecipe6.ResumeLayout(false);
            this.groupBoxRecipe6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxRecipe1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelName1;
        private System.Windows.Forms.PictureBox pictureBoxRecipe1;
        private System.Windows.Forms.Label labelType1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTimeToPrepare1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBoxRecipe2;
        private System.Windows.Forms.Label labelTimeToPrepare2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelType2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelName2;
        private System.Windows.Forms.PictureBox pictureBoxRecipe2;
        private System.Windows.Forms.GroupBox groupBoxRecipe3;
        private System.Windows.Forms.Label labelTimeToPrepare3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelType3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelName3;
        private System.Windows.Forms.PictureBox pictureBoxRecipe3;
        private System.Windows.Forms.GroupBox groupBoxRecipe4;
        private System.Windows.Forms.Label labelTimeToPrepare4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelType4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelName4;
        private System.Windows.Forms.PictureBox pictureBoxRecipe4;
        private System.Windows.Forms.GroupBox groupBoxRecipe5;
        private System.Windows.Forms.Label labelTimeToPrepare5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelType5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelName5;
        private System.Windows.Forms.PictureBox pictureBoxRecipe5;
        private System.Windows.Forms.GroupBox groupBoxRecipe6;
        private System.Windows.Forms.Label labelTimeToPrepare6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelType6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelName6;
        private System.Windows.Forms.PictureBox pictureBoxRecipe6;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.Button buttonRate1;
        private System.Windows.Forms.Button buttonRate2;
        private System.Windows.Forms.Button buttonRate3;
        private System.Windows.Forms.Button buttonRate4;
        private System.Windows.Forms.Button buttonRate5;
        private System.Windows.Forms.Button buttonRate6;
    }
}