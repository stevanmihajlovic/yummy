﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class AddIngredientForm : Form
    {
        GraphClient client;

        public AddIngredientForm()
        {
            InitializeComponent();
        }

        public AddIngredientForm(GraphClient client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "" && typeComboBox.SelectedIndex != -1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Ingredient) WHERE n.name =~ '" + nameTextBox.Text + "' return n;",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
                Ingredient check = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList().FirstOrDefault();

                if (check != null)
                    MessageBox.Show("Ingredient with this name already exists!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    string maxId = getMaxId("Ingredient");
                    int id;
                    try
                    {
                        int mId = int.Parse(maxId);
                        id = (++mId);
                    }
                    catch (Exception exception)
                    {
                        id = 1;
                    }

                    Ingredient ingredient = new Ingredient(id, nameTextBox.Text, typeComboBox.SelectedItem.ToString());

                    ingredient.createIngredient(client);
                    
                    Close();
                }
            }
            else
                MessageBox.Show("Please fill in all the fields!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + s + ") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}