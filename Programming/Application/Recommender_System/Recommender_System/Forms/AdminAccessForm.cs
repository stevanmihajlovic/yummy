﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class AdminAccessForm : Form
    {
        GraphClient client;

        public AdminAccessForm()
        {
            InitializeComponent();
        }

        public AdminAccessForm(GraphClient client)
        {
            InitializeComponent();
            this.client = client;
        }

        private void addIngredientButton_Click(object sender, EventArgs e)
        {
            Hide();
            AddIngredientForm form = new AddIngredientForm(client);
            form.ShowDialog();
            Show();
        }

        private void modifyIngredientButton_Click(object sender, EventArgs e)
        {
            Hide();
            ModifyIngredientForm form = new ModifyIngredientForm(client);
            form.ShowDialog();
            Show();
        }

        private void buttonAddRecipe_Click(object sender, EventArgs e)
        {
            Hide();
            AddRecipeForm form = new AddRecipeForm(client);
            form.ShowDialog();
            Show();
        }

        private void buttonModifyRecipe_Click(object sender, EventArgs e)
        {
            Hide();
            ModifyRecipeForm form = new ModifyRecipeForm(client);
            form.ShowDialog();
            Show();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
