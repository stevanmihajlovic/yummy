﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class SelectIngredientsForm : Form
    {
        GraphClient client;
        String mode;
        User user;
        List<Ingredient> ingredients;
        String selectedType;

        public SelectIngredientsForm()
        {
            InitializeComponent();
        }
        public SelectIngredientsForm(GraphClient client, String mode, User user)
        {
            InitializeComponent();
            this.client=client;
            this.mode = mode;
            this.user = user;
            this.ingredients = new List<Ingredient>();

            if (mode.Equals("alergic"))
            {
                labelLeft.Text = "Add ingredients you are alergic to:";
                labelRight.Text = "Alergic to:";
            }
            else
            {
                labelLeft.Text = "Add ingredients you like:";
                labelRight.Text = "Liked:";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedType = comboBox1.SelectedItem.ToString();
            fillLeftListbox();            
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                listBox2.Items.Add(listBox1.SelectedItem);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                listBox1.SelectedIndex = -1;
            }
            else
                MessageBox.Show("Please select ingredient you wish to add first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                fillLeftListbox();
            }
            else
                MessageBox.Show("Please select ingredient you wish to remove first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void fillLeftListbox()
        {
            listBox1.Items.Clear();
            ingredients.Clear();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Ingredient) where n.type = '" + selectedType + "' return n",
                                                                    new Dictionary<string, object>(), CypherResultMode.Set);
            ingredients = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList();

            foreach (Ingredient i in ingredients)
                if (!listBox2.Items.Contains(i.name))
                    listBox1.Items.Add(i.name);
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            if (listBox2.Items.Count > 0)
                foreach (string name in listBox2.Items)
                    connectNodes(name);
            Hide();
            SelectRecipesForm form = new SelectRecipesForm(client, user);
            form.ShowDialog();
            Close();
        }
        
        private void connectNodes(string ingredientName)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a: User), (b:Ingredient) WHERE a.username =~ '" + user.username
                                                                        + "' AND b.name =~ '" + ingredientName + 
                                                                        "' CREATE(a)-[r: " + mode + "]->(b) RETURN r",
                                                                        new Dictionary<string, object>(),
                                                                        CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }
    }
}