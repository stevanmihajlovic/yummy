﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class RateRecipeForm : Form
    {
        GraphClient client;
        User user;
        Recipe recipe;
        public int stars;

        public RateRecipeForm(GraphClient client, User user, Recipe recipe)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;
            this.recipe = recipe;

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("id_user", user.username);
            dictionary.Add("id_recipe", recipe.name);
            var query = new CypherQuery("MATCH (n:User), (m:Recipe), n-[r:rated]->m WHERE n.username = {id_user} AND m.name = {id_recipe} return r.rate", dictionary, CypherResultMode.Set);
            stars = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).FirstOrDefault();
            switch (stars)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        pictureBoxEmpty1.Visible = false;
                        pictureBoxFull1.Visible = true;
                        break;
                    }
                case 2:
                    {
                        pictureBoxEmpty1.Visible = false;
                        pictureBoxFull1.Visible = true;
                        pictureBoxEmpty2.Visible = false;
                        pictureBoxFull2.Visible = true;
                        break;
                    }
                case 3:
                    {
                        pictureBoxEmpty1.Visible = false;
                        pictureBoxFull1.Visible = true;
                        pictureBoxEmpty2.Visible = false;
                        pictureBoxFull2.Visible = true;
                        pictureBoxEmpty3.Visible = false;
                        pictureBoxFull3.Visible = true;
                        break;
                    }
                case 4:
                    {
                        pictureBoxEmpty1.Visible = false;
                        pictureBoxFull1.Visible = true;
                        pictureBoxEmpty2.Visible = false;
                        pictureBoxFull2.Visible = true;
                        pictureBoxEmpty3.Visible = false;
                        pictureBoxFull3.Visible = true;
                        pictureBoxEmpty4.Visible = false;
                        pictureBoxFull4.Visible = true;
                        break;
                    }
                case 5:
                    {
                        pictureBoxEmpty1.Visible = false;
                        pictureBoxFull1.Visible = true;
                        pictureBoxEmpty2.Visible = false;
                        pictureBoxFull2.Visible = true;
                        pictureBoxEmpty3.Visible = false;
                        pictureBoxFull3.Visible = true;
                        pictureBoxEmpty4.Visible = false;
                        pictureBoxFull4.Visible = true;
                        pictureBoxEmpty5.Visible = false;
                        pictureBoxFull5.Visible = true;
                        break;
                    }
            }
        }

        private void createRatedRelationship(int rate)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("id_user", user.username);
            dictionary.Add("id_recipe", recipe.name);
            dictionary.Add("stars", rate);
            var query = new CypherQuery("MATCH (n:User), (m:Recipe) WHERE n.username = {id_user} and m.name = {id_recipe} CREATE n-[:rated {rate: {stars}}]->m", dictionary, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        private void updateRatedRelationship(int rate)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("id_user", user.username);
            dictionary.Add("id_recipe", recipe.name);
            dictionary.Add("stars", rate);
            var query = new CypherQuery("MATCH (n:User), (m:Recipe), n-[r:rated]->m WHERE n.username = {id_user} and m.name = {id_recipe} SET r.rate = {stars}", dictionary, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        private void pictureBoxEmpty1_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxEmpty1.Visible = false;
            pictureBoxFull1.Visible = true;
        }

        private void pictureBoxFull1_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxFull1.Visible = false;
            pictureBoxEmpty1.Visible = true;
        }

        private void pictureBoxEmpty2_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxEmpty1.Visible = false;
            pictureBoxFull1.Visible = true;
            pictureBoxEmpty2.Visible = false;
            pictureBoxFull2.Visible = true;
        }

        private void pictureBoxFull2_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxFull1.Visible = false;
            pictureBoxEmpty1.Visible = true;
            pictureBoxFull2.Visible = false;
            pictureBoxEmpty2.Visible = true;
        }

        private void pictureBoxEmpty3_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxEmpty1.Visible = false;
            pictureBoxFull1.Visible = true;
            pictureBoxEmpty2.Visible = false;
            pictureBoxFull2.Visible = true;
            pictureBoxEmpty3.Visible = false;
            pictureBoxFull3.Visible = true;
        }

        private void pictureBoxFull3_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxFull1.Visible = false;
            pictureBoxEmpty1.Visible = true;
            pictureBoxFull2.Visible = false;
            pictureBoxEmpty2.Visible = true;
            pictureBoxFull3.Visible = false;
            pictureBoxEmpty3.Visible = true;
        }

        private void pictureBoxEmpty4_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxEmpty1.Visible = false;
            pictureBoxFull1.Visible = true;
            pictureBoxEmpty2.Visible = false;
            pictureBoxFull2.Visible = true;
            pictureBoxEmpty3.Visible = false;
            pictureBoxFull3.Visible = true;
            pictureBoxEmpty4.Visible = false;
            pictureBoxFull4.Visible = true;
        }

        private void pictureBoxFull4_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxFull1.Visible = false;
            pictureBoxEmpty1.Visible = true;
            pictureBoxFull2.Visible = false;
            pictureBoxEmpty2.Visible = true;
            pictureBoxFull3.Visible = false;
            pictureBoxEmpty3.Visible = true;
            pictureBoxFull4.Visible = false;
            pictureBoxEmpty4.Visible = true;
        }

        private void pictureBoxEmpty5_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxEmpty1.Visible = false;
            pictureBoxFull1.Visible = true;
            pictureBoxEmpty2.Visible = false;
            pictureBoxFull2.Visible = true;
            pictureBoxEmpty3.Visible = false;
            pictureBoxFull3.Visible = true;
            pictureBoxEmpty4.Visible = false;
            pictureBoxFull4.Visible = true;
            pictureBoxEmpty5.Visible = false;
            pictureBoxFull5.Visible = true;
        }

        private void pictureBoxFull5_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxFull1.Visible = false;
            pictureBoxEmpty1.Visible = true;
            pictureBoxFull2.Visible = false;
            pictureBoxEmpty2.Visible = true;
            pictureBoxFull3.Visible = false;
            pictureBoxEmpty3.Visible = true;
            pictureBoxFull4.Visible = false;
            pictureBoxEmpty4.Visible = true;
            pictureBoxFull5.Visible = false;
            pictureBoxEmpty5.Visible = true;
        }

        private void pictureBoxFull1_Click(object sender, EventArgs e)
        {
            if (stars != 0)
                updateRatedRelationship(1);
            else
                createRatedRelationship(1);
            stars = 1;
            Close();
        }

        private void pictureBoxFull2_Click(object sender, EventArgs e)
        {
            if (stars != 0)
                updateRatedRelationship(2);
            else
                createRatedRelationship(2);
            stars = 2;
            Close();
        }

        private void pictureBoxFull3_Click(object sender, EventArgs e)
        {
            if (stars != 0)
                updateRatedRelationship(3);
            else
                createRatedRelationship(3);
            stars = 3;
            Close();
        }

        private void pictureBoxFull4_Click(object sender, EventArgs e)
        {
            if (stars != 0)
                updateRatedRelationship(4);
            else
                createRatedRelationship(4);
            stars = 4;
            Close();
        }

        private void pictureBoxFull5_Click(object sender, EventArgs e)
        {
            if (stars != 0)
                updateRatedRelationship(5);
            else
                createRatedRelationship(5);
            stars = 5;
            Close();
        }
    }
}