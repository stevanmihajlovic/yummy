﻿namespace Recommender_System.Forms
{
    partial class AdminAccessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addIngredientButton = new System.Windows.Forms.Button();
            this.modifyIngredientButton = new System.Windows.Forms.Button();
            this.buttonAddRecipe = new System.Windows.Forms.Button();
            this.buttonModifyRecipe = new System.Windows.Forms.Button();
            this.groupBoxIngredient = new System.Windows.Forms.GroupBox();
            this.groupBoxRecipe = new System.Windows.Forms.GroupBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.groupBoxIngredient.SuspendLayout();
            this.groupBoxRecipe.SuspendLayout();
            this.SuspendLayout();
            // 
            // addIngredientButton
            // 
            this.addIngredientButton.Location = new System.Drawing.Point(44, 50);
            this.addIngredientButton.Name = "addIngredientButton";
            this.addIngredientButton.Size = new System.Drawing.Size(170, 49);
            this.addIngredientButton.TabIndex = 0;
            this.addIngredientButton.TabStop = false;
            this.addIngredientButton.Text = "Add Ingredient";
            this.addIngredientButton.UseVisualStyleBackColor = true;
            this.addIngredientButton.Click += new System.EventHandler(this.addIngredientButton_Click);
            // 
            // modifyIngredientButton
            // 
            this.modifyIngredientButton.Location = new System.Drawing.Point(44, 105);
            this.modifyIngredientButton.Name = "modifyIngredientButton";
            this.modifyIngredientButton.Size = new System.Drawing.Size(170, 49);
            this.modifyIngredientButton.TabIndex = 1;
            this.modifyIngredientButton.TabStop = false;
            this.modifyIngredientButton.Text = "Modify Ingredient";
            this.modifyIngredientButton.UseVisualStyleBackColor = true;
            this.modifyIngredientButton.Click += new System.EventHandler(this.modifyIngredientButton_Click);
            // 
            // buttonAddRecipe
            // 
            this.buttonAddRecipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddRecipe.Location = new System.Drawing.Point(44, 50);
            this.buttonAddRecipe.Name = "buttonAddRecipe";
            this.buttonAddRecipe.Size = new System.Drawing.Size(170, 42);
            this.buttonAddRecipe.TabIndex = 2;
            this.buttonAddRecipe.TabStop = false;
            this.buttonAddRecipe.Text = "Add Recipe";
            this.buttonAddRecipe.UseVisualStyleBackColor = true;
            this.buttonAddRecipe.Click += new System.EventHandler(this.buttonAddRecipe_Click);
            // 
            // buttonModifyRecipe
            // 
            this.buttonModifyRecipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonModifyRecipe.Location = new System.Drawing.Point(44, 98);
            this.buttonModifyRecipe.Name = "buttonModifyRecipe";
            this.buttonModifyRecipe.Size = new System.Drawing.Size(170, 42);
            this.buttonModifyRecipe.TabIndex = 3;
            this.buttonModifyRecipe.TabStop = false;
            this.buttonModifyRecipe.Text = "Modify Recipe";
            this.buttonModifyRecipe.UseVisualStyleBackColor = true;
            this.buttonModifyRecipe.Click += new System.EventHandler(this.buttonModifyRecipe_Click);
            // 
            // groupBoxIngredient
            // 
            this.groupBoxIngredient.Controls.Add(this.modifyIngredientButton);
            this.groupBoxIngredient.Controls.Add(this.addIngredientButton);
            this.groupBoxIngredient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxIngredient.Location = new System.Drawing.Point(12, 205);
            this.groupBoxIngredient.Name = "groupBoxIngredient";
            this.groupBoxIngredient.Size = new System.Drawing.Size(258, 187);
            this.groupBoxIngredient.TabIndex = 4;
            this.groupBoxIngredient.TabStop = false;
            this.groupBoxIngredient.Text = "Ingredient";
            // 
            // groupBoxRecipe
            // 
            this.groupBoxRecipe.Controls.Add(this.buttonAddRecipe);
            this.groupBoxRecipe.Controls.Add(this.buttonModifyRecipe);
            this.groupBoxRecipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRecipe.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRecipe.Name = "groupBoxRecipe";
            this.groupBoxRecipe.Size = new System.Drawing.Size(258, 187);
            this.groupBoxRecipe.TabIndex = 5;
            this.groupBoxRecipe.TabStop = false;
            this.groupBoxRecipe.Text = "Recipe";
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(148, 398);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(122, 49);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.TabStop = false;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // AdminAccessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 456);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.groupBoxRecipe);
            this.Controls.Add(this.groupBoxIngredient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AdminAccessForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            this.groupBoxIngredient.ResumeLayout(false);
            this.groupBoxRecipe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addIngredientButton;
        private System.Windows.Forms.Button modifyIngredientButton;
        private System.Windows.Forms.Button buttonAddRecipe;
        private System.Windows.Forms.Button buttonModifyRecipe;
        private System.Windows.Forms.GroupBox groupBoxIngredient;
        private System.Windows.Forms.GroupBox groupBoxRecipe;
        private System.Windows.Forms.Button buttonClose;
    }
}