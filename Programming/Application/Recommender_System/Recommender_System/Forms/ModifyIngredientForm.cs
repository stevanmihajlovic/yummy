﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class ModifyIngredientForm : Form
    {
        GraphClient client;
        List<Ingredient> ingredients;

        public ModifyIngredientForm()
        {
            InitializeComponent();
        }

        public ModifyIngredientForm(GraphClient client)
        {
            InitializeComponent();
            this.client = client;

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Ingredient) RETURN n",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            ingredients = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList();

            foreach (Ingredient i in ingredients)
                listBox1.Items.Add(i.name + "\t" + i.type);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                return;
            else
            {
                nameTextBox.Text = ingredients[listBox1.SelectedIndex].name;
                typeComboBox.SelectedIndex = typeComboBox.Items.IndexOf(ingredients[listBox1.SelectedIndex].type);
            }
        }

        private void modifyButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                MessageBox.Show("Please select the ingredient you wish to modify first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (nameTextBox.Text != "" && typeComboBox.SelectedIndex != -1)
                {
                    Ingredient chosen = ingredients[listBox1.SelectedIndex];
                    chosen.name = nameTextBox.Text;
                    chosen.type = typeComboBox.SelectedItem.ToString();

                    chosen.updateIngredient(client);

                    int index = listBox1.SelectedIndex;
                    listBox1.Items[index] = chosen.name + "\t" + chosen.type;
                }
                else
                    MessageBox.Show("Please fill in all the fields!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                MessageBox.Show("Please select the ingredient you wish to delete first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                ingredients[listBox1.SelectedIndex].deleteIngredient(client);
                ingredients.RemoveAt(listBox1.SelectedIndex);
                nameTextBox.Text = "";
                typeComboBox.SelectedIndex = -1;
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}