﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recommender_System.Forms
{
    public partial class RecipeForm : Form
    {
        GraphClient client;
        User user;
        Recipe recipe;
        List<Recipe> unratedRecipes;
        List<Recipe> ratedRecipes;
        List<User> similarUsers;
        List<Recipe> topRecommended;
        float averageRating;

        List<float> similarities;
        List<float> predictions;

        public RecipeForm(GraphClient client, User user, Recipe recipe)
        {
            this.client = client;
            this.user = user;
            this.recipe = recipe;
            averageRating = 0;
            unratedRecipes = new List<Recipe>();
            ratedRecipes = new List<Recipe>();
            similarUsers = new List<User>();
            similarities = new List<float>();
            predictions = new List<float>();
            topRecommended = new List<Recipe>();
            InitializeComponent();

            if (checkAlergic(recipe))
                pictureBoxAlergic.Visible = true;

            pictureBoxRecipe.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(recipe.name.Replace(' ', '_').ToLower());
            labelName1.Text = recipe.name;
            labelType1.Text = recipe.type;
            labelTimeToPrepare1.Text = recipe.timeToPrepare;
            textBoxInstructions.Text = recipe.instructions;
            averageRating =  userAverageRating(user, out ratedRecipes);
            calculateRecommendations();
        }

        private void calculateRecommendations()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User), (m:Recipe) WHERE n.username = '" + user.username
                                                            + "' AND m.name <> '" + recipe.name + "' AND NOT(n-[:rated]->m) RETURN DISTINCT m",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            List<Recipe> tempRecipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();
            unratedRecipes.Clear();
            foreach (Recipe r in tempRecipes)
            {
                if (!checkAlergic(r))
                    unratedRecipes.Add(r);
            }

            query = new Neo4jClient.Cypher.CypherQuery("MATCH(n: User), (m:User), (r:Recipe) WHERE n.username = '" + user.username
                                                        + "' AND m.username <> n.username AND n-[:rated]->r AND m-[:rated]->r RETURN DISTINCT m",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            similarUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList();

            foreach (Recipe r in unratedRecipes)
            {
                List<Recipe> commonRatedRecipes;
                float topExpression = 0;
                foreach (User u in similarUsers)
                {
                    int rate = getRating(u, r);
                    if (rate != 0)
                    {
                        List<Recipe> otherUserRatedRecipes = new List<Recipe>();
                        float averageOther = userAverageRating(u, out otherUserRatedRecipes);
                        commonRatedRecipes = new List<Recipe>(ratedRecipes);
                        for (int i = 0; i < commonRatedRecipes.Count; i++)
                        {
                            int count = 0;
                            foreach (Recipe rec in otherUserRatedRecipes)
                            {
                                if (rec.name != commonRatedRecipes[i].name)
                                    count++;
                                else
                                    break;
                            }
                            if (count == otherUserRatedRecipes.Count)
                            {
                                commonRatedRecipes.RemoveAt(i);
                                i--;
                            }
                        }

                        float sumTop = 0, sumLeft = 0, sumRight = 0;
                        foreach (Recipe rec in commonRatedRecipes)
                        {
                            float leftParenthesis = getRating(user, rec) - averageRating;
                            float rightParenthesis = getRating(u, rec) - averageOther;
                            sumTop += leftParenthesis * rightParenthesis;
                            sumLeft += leftParenthesis * leftParenthesis;
                            sumRight += rightParenthesis * rightParenthesis;
                        }
                        float similarity = sumTop / (float)(Math.Sqrt(sumLeft) * Math.Sqrt(sumRight));
                        similarities.Add(similarity);
                        
                        List<Recipe> list = new List<Recipe>();
                        topExpression += similarity * (getRating(u, r) - userAverageRating(u, out list));
                    }
                }
                float prediction = averageRating + topExpression / similarities.Sum();
                predictions.Add(prediction);
            }

            fillRecommendations();
        }

        private void buttonRate1_Click(object sender, EventArgs e)
        {
            RateRecipeForm form = new RateRecipeForm(client, user, recipe);
            form.ShowDialog();
            averageRating = userAverageRating(user, out ratedRecipes);
        }

        private float userAverageRating(User u, out List<Recipe> rcps)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User), (m:Recipe) WHERE n.username = '" + u.username
                                                               + "' AND (n-[:rated]->m) RETURN m",
                                                             new Dictionary<string, object>(), CypherResultMode.Set);
            rcps = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();

            query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User),  (n-[r:rated]->m) WHERE n.username = '" + u.username
                                                               + "' RETURN SUM(r.rate)",
                                                             new Dictionary<string, object>(), CypherResultMode.Set);
            return ((IRawGraphClient)client).ExecuteGetCypherResults<float>(query).FirstOrDefault() / rcps.Count;
        }

        private int getRating(User u, Recipe r)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User), (r:Recipe), n-[m:rated]->r WHERE n.username = '" + u.username
                                                        + "' AND r.name = '" + r.name + "' RETURN m.rate",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            return ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).FirstOrDefault();
        }

        private void fillRecommendations()
        {
            int index = predictions.IndexOf(predictions.Max());
            labelRecipeName1.Text = unratedRecipes[index].name;
            pictureBoxRecipe1.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(unratedRecipes[index].name.Replace(' ', '_').ToLower());
            topRecommended.Add(unratedRecipes[index]);
            predictions.RemoveAt(index);
            unratedRecipes.RemoveAt(index);

            index = predictions.IndexOf(predictions.Max());
            labelRecipeName2.Text = unratedRecipes[index].name;
            pictureBoxRecipe2.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(unratedRecipes[index].name.Replace(' ', '_').ToLower());
            topRecommended.Add(unratedRecipes[index]);
            predictions.RemoveAt(index);
            unratedRecipes.RemoveAt(index);

            index = predictions.IndexOf(predictions.Max());
            labelRecipeName3.Text = unratedRecipes[index].name;
            pictureBoxRecipe3.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject(unratedRecipes[index].name.Replace(' ', '_').ToLower());
            topRecommended.Add(unratedRecipes[index]);
            predictions.RemoveAt(index);
            unratedRecipes.RemoveAt(index);
        }

        private bool checkAlergic(Recipe rcpt)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User), (r:Recipe), (i:Ingredient) WHERE n.username = '" + user.username
                                                        + "' AND r.name = '" + rcpt.name + "' AND i-[:partOf]->r AND n-[:alergic]->i RETURN i",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            Ingredient alergic = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).FirstOrDefault();
            if (alergic != null)
                return true;
            return false;
        }

        private void pictureBoxRecipe1_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, topRecommended[0]);
            form.ShowDialog();
            Close();
        }

        private void pictureBoxRecipe2_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, topRecommended[1]);
            form.ShowDialog();
            Close();
        }

        private void pictureBoxRecipe3_Click(object sender, EventArgs e)
        {
            Hide();
            RecipeForm form = new RecipeForm(client, user, topRecommended[2]);
            form.ShowDialog();
            Close();
        }
    }
}