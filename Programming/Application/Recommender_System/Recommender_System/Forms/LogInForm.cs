﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class LogInForm : Form
    {
        GraphClient client;

        public LogInForm()
        {
            InitializeComponent();

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "12345");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            string username = usernameTextBox.Text;
            string password = passwordTextBox.Text;
            if (username == "admin" && password == "admin")
            {
                Hide();
                AdminAccessForm form = new AdminAccessForm(client);
                form.ShowDialog();
                Show();
            }
            else
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User) WHERE n.username =~'" + usernameTextBox.Text
                                                   + "' and n.password =~'" + passwordTextBox.Text + "' return n;",
                                                   new Dictionary<string, object>(), CypherResultMode.Set);

                User user = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList().FirstOrDefault();

                if (user == null)
                    MessageBox.Show("Wrong username or password!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Hide();
                    MainForm form = new MainForm(client, user);
                    form.ShowDialog();
                    Close();
                }
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void linkLabelRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            RegisterForm form = new RegisterForm(client);
            form.ShowDialog();
            Close();
        }
    }
}