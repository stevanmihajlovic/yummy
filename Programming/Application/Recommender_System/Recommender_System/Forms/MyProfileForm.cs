﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class MyProfileForm : Form
    {
        GraphClient client;
        User user;

        public MyProfileForm(GraphClient client, User user)
        {
            InitializeComponent();

            this.client = client;
            this.user = user;

            labelUsername.Text = user.username;
            labelFirstName.Text = user.firstName;
            labelLastName.Text = user.lastName;
            labelAge.Text = user.age.ToString();
            labelCountry.Text = user.country;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonEditProfile_Click(object sender, EventArgs e)
        {
            EditProfileForm form = new EditProfileForm(client, user);
            form.ShowDialog();
            user.firstName = form.user.firstName;
            user.lastName = form.user.lastName;
            user.password = form.user.password;
            user.age = form.user.age;
            user.country = form.user.country;
            labelFirstName.Text = user.firstName;
            labelLastName.Text = user.lastName;
            labelAge.Text = user.age.ToString();
            labelCountry.Text = user.country;
        }

        private void buttonRatings_Click(object sender, EventArgs e)
        {
            MyRatingsForm form = new MyRatingsForm(client, user);
            form.ShowDialog();
        }
    }
}