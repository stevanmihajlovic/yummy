﻿namespace Recommender_System.Forms
{
    partial class MyRatingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.groupBoxRatings = new System.Windows.Forms.GroupBox();
            this.labelRating6 = new System.Windows.Forms.Label();
            this.labelRating5 = new System.Windows.Forms.Label();
            this.labelRating4 = new System.Windows.Forms.Label();
            this.labelRating3 = new System.Windows.Forms.Label();
            this.labelRating2 = new System.Windows.Forms.Label();
            this.labelRating1 = new System.Windows.Forms.Label();
            this.buttonChange6 = new System.Windows.Forms.Button();
            this.buttonChange5 = new System.Windows.Forms.Button();
            this.buttonChange4 = new System.Windows.Forms.Button();
            this.labelRatingTitle6 = new System.Windows.Forms.Label();
            this.labelName6 = new System.Windows.Forms.Label();
            this.labelRatingTitle5 = new System.Windows.Forms.Label();
            this.labelName5 = new System.Windows.Forms.Label();
            this.labelRatingTitle4 = new System.Windows.Forms.Label();
            this.labelName4 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe6 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe4 = new System.Windows.Forms.PictureBox();
            this.buttonChange3 = new System.Windows.Forms.Button();
            this.buttonChange2 = new System.Windows.Forms.Button();
            this.buttonChange1 = new System.Windows.Forms.Button();
            this.labelRatingTitle3 = new System.Windows.Forms.Label();
            this.labelName3 = new System.Windows.Forms.Label();
            this.labelRatingTitle2 = new System.Windows.Forms.Label();
            this.labelName2 = new System.Windows.Forms.Label();
            this.labelRatingTitle1 = new System.Windows.Forms.Label();
            this.labelName1 = new System.Windows.Forms.Label();
            this.pictureBoxRecipe3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRecipe1 = new System.Windows.Forms.PictureBox();
            this.groupBoxRatings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.Location = new System.Drawing.Point(11, 479);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(91, 37);
            this.buttonClose.TabIndex = 51;
            this.buttonClose.TabStop = false;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.Location = new System.Drawing.Point(806, 479);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(91, 37);
            this.buttonNext.TabIndex = 50;
            this.buttonNext.TabStop = false;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrevious.Location = new System.Drawing.Point(709, 479);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(91, 37);
            this.buttonPrevious.TabIndex = 49;
            this.buttonPrevious.TabStop = false;
            this.buttonPrevious.Text = "Previous";
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.buttonPrevious_Click);
            // 
            // groupBoxRatings
            // 
            this.groupBoxRatings.Controls.Add(this.labelRating6);
            this.groupBoxRatings.Controls.Add(this.labelRating5);
            this.groupBoxRatings.Controls.Add(this.labelRating4);
            this.groupBoxRatings.Controls.Add(this.labelRating3);
            this.groupBoxRatings.Controls.Add(this.labelRating2);
            this.groupBoxRatings.Controls.Add(this.labelRating1);
            this.groupBoxRatings.Controls.Add(this.buttonChange6);
            this.groupBoxRatings.Controls.Add(this.buttonChange5);
            this.groupBoxRatings.Controls.Add(this.buttonChange4);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle6);
            this.groupBoxRatings.Controls.Add(this.labelName6);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle5);
            this.groupBoxRatings.Controls.Add(this.labelName5);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle4);
            this.groupBoxRatings.Controls.Add(this.labelName4);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe6);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe5);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe4);
            this.groupBoxRatings.Controls.Add(this.buttonChange3);
            this.groupBoxRatings.Controls.Add(this.buttonChange2);
            this.groupBoxRatings.Controls.Add(this.buttonChange1);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle3);
            this.groupBoxRatings.Controls.Add(this.labelName3);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle2);
            this.groupBoxRatings.Controls.Add(this.labelName2);
            this.groupBoxRatings.Controls.Add(this.labelRatingTitle1);
            this.groupBoxRatings.Controls.Add(this.labelName1);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe3);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe2);
            this.groupBoxRatings.Controls.Add(this.pictureBoxRecipe1);
            this.groupBoxRatings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRatings.Location = new System.Drawing.Point(11, -1);
            this.groupBoxRatings.Name = "groupBoxRatings";
            this.groupBoxRatings.Size = new System.Drawing.Size(886, 474);
            this.groupBoxRatings.TabIndex = 48;
            this.groupBoxRatings.TabStop = false;
            // 
            // labelRating6
            // 
            this.labelRating6.AutoSize = true;
            this.labelRating6.Location = new System.Drawing.Point(647, 401);
            this.labelRating6.Name = "labelRating6";
            this.labelRating6.Size = new System.Drawing.Size(60, 24);
            this.labelRating6.TabIndex = 43;
            this.labelRating6.Text = "label1";
            // 
            // labelRating5
            // 
            this.labelRating5.AutoSize = true;
            this.labelRating5.Location = new System.Drawing.Point(647, 257);
            this.labelRating5.Name = "labelRating5";
            this.labelRating5.Size = new System.Drawing.Size(60, 24);
            this.labelRating5.TabIndex = 42;
            this.labelRating5.Text = "label1";
            // 
            // labelRating4
            // 
            this.labelRating4.AutoSize = true;
            this.labelRating4.Location = new System.Drawing.Point(647, 111);
            this.labelRating4.Name = "labelRating4";
            this.labelRating4.Size = new System.Drawing.Size(60, 24);
            this.labelRating4.TabIndex = 41;
            this.labelRating4.Text = "label1";
            // 
            // labelRating3
            // 
            this.labelRating3.AutoSize = true;
            this.labelRating3.Location = new System.Drawing.Point(178, 401);
            this.labelRating3.Name = "labelRating3";
            this.labelRating3.Size = new System.Drawing.Size(60, 24);
            this.labelRating3.TabIndex = 40;
            this.labelRating3.Text = "label1";
            // 
            // labelRating2
            // 
            this.labelRating2.AutoSize = true;
            this.labelRating2.Location = new System.Drawing.Point(178, 257);
            this.labelRating2.Name = "labelRating2";
            this.labelRating2.Size = new System.Drawing.Size(60, 24);
            this.labelRating2.TabIndex = 39;
            this.labelRating2.Text = "label1";
            // 
            // labelRating1
            // 
            this.labelRating1.AutoSize = true;
            this.labelRating1.Location = new System.Drawing.Point(178, 111);
            this.labelRating1.Name = "labelRating1";
            this.labelRating1.Size = new System.Drawing.Size(60, 24);
            this.labelRating1.TabIndex = 38;
            this.labelRating1.Text = "label1";
            // 
            // buttonChange6
            // 
            this.buttonChange6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange6.Location = new System.Drawing.Point(477, 431);
            this.buttonChange6.Name = "buttonChange6";
            this.buttonChange6.Size = new System.Drawing.Size(74, 32);
            this.buttonChange6.TabIndex = 37;
            this.buttonChange6.TabStop = false;
            this.buttonChange6.Text = "Change";
            this.buttonChange6.UseVisualStyleBackColor = true;
            this.buttonChange6.Click += new System.EventHandler(this.buttonChange6_Click);
            // 
            // buttonChange5
            // 
            this.buttonChange5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange5.Location = new System.Drawing.Point(477, 285);
            this.buttonChange5.Name = "buttonChange5";
            this.buttonChange5.Size = new System.Drawing.Size(74, 32);
            this.buttonChange5.TabIndex = 36;
            this.buttonChange5.TabStop = false;
            this.buttonChange5.Text = "Change";
            this.buttonChange5.UseVisualStyleBackColor = true;
            this.buttonChange5.Click += new System.EventHandler(this.buttonChange5_Click);
            // 
            // buttonChange4
            // 
            this.buttonChange4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange4.Location = new System.Drawing.Point(477, 141);
            this.buttonChange4.Name = "buttonChange4";
            this.buttonChange4.Size = new System.Drawing.Size(74, 32);
            this.buttonChange4.TabIndex = 35;
            this.buttonChange4.TabStop = false;
            this.buttonChange4.Text = "Change";
            this.buttonChange4.UseVisualStyleBackColor = true;
            this.buttonChange4.Click += new System.EventHandler(this.buttonChange4_Click);
            // 
            // labelRatingTitle6
            // 
            this.labelRatingTitle6.AutoSize = true;
            this.labelRatingTitle6.Location = new System.Drawing.Point(573, 401);
            this.labelRatingTitle6.Name = "labelRatingTitle6";
            this.labelRatingTitle6.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle6.TabIndex = 30;
            this.labelRatingTitle6.Text = "Rating:";
            // 
            // labelName6
            // 
            this.labelName6.AutoSize = true;
            this.labelName6.Location = new System.Drawing.Point(573, 329);
            this.labelName6.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName6.Name = "labelName6";
            this.labelName6.Size = new System.Drawing.Size(60, 24);
            this.labelName6.TabIndex = 29;
            this.labelName6.Text = "label1";
            // 
            // labelRatingTitle5
            // 
            this.labelRatingTitle5.AutoSize = true;
            this.labelRatingTitle5.Location = new System.Drawing.Point(573, 257);
            this.labelRatingTitle5.Name = "labelRatingTitle5";
            this.labelRatingTitle5.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle5.TabIndex = 28;
            this.labelRatingTitle5.Text = "Rating:";
            // 
            // labelName5
            // 
            this.labelName5.AutoSize = true;
            this.labelName5.Location = new System.Drawing.Point(573, 183);
            this.labelName5.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName5.Name = "labelName5";
            this.labelName5.Size = new System.Drawing.Size(60, 24);
            this.labelName5.TabIndex = 27;
            this.labelName5.Text = "label1";
            // 
            // labelRatingTitle4
            // 
            this.labelRatingTitle4.AutoSize = true;
            this.labelRatingTitle4.Location = new System.Drawing.Point(573, 111);
            this.labelRatingTitle4.Name = "labelRatingTitle4";
            this.labelRatingTitle4.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle4.TabIndex = 26;
            this.labelRatingTitle4.Text = "Rating:";
            // 
            // labelName4
            // 
            this.labelName4.AutoSize = true;
            this.labelName4.Location = new System.Drawing.Point(573, 39);
            this.labelName4.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName4.Name = "labelName4";
            this.labelName4.Size = new System.Drawing.Size(60, 24);
            this.labelName4.TabIndex = 25;
            this.labelName4.Text = "label1";
            // 
            // pictureBoxRecipe6
            // 
            this.pictureBoxRecipe6.Location = new System.Drawing.Point(477, 329);
            this.pictureBoxRecipe6.Name = "pictureBoxRecipe6";
            this.pictureBoxRecipe6.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe6.TabIndex = 22;
            this.pictureBoxRecipe6.TabStop = false;
            // 
            // pictureBoxRecipe5
            // 
            this.pictureBoxRecipe5.Location = new System.Drawing.Point(477, 183);
            this.pictureBoxRecipe5.Name = "pictureBoxRecipe5";
            this.pictureBoxRecipe5.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe5.TabIndex = 21;
            this.pictureBoxRecipe5.TabStop = false;
            // 
            // pictureBoxRecipe4
            // 
            this.pictureBoxRecipe4.Location = new System.Drawing.Point(477, 39);
            this.pictureBoxRecipe4.Name = "pictureBoxRecipe4";
            this.pictureBoxRecipe4.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe4.TabIndex = 20;
            this.pictureBoxRecipe4.TabStop = false;
            // 
            // buttonChange3
            // 
            this.buttonChange3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange3.Location = new System.Drawing.Point(11, 431);
            this.buttonChange3.Name = "buttonChange3";
            this.buttonChange3.Size = new System.Drawing.Size(74, 32);
            this.buttonChange3.TabIndex = 17;
            this.buttonChange3.TabStop = false;
            this.buttonChange3.Text = "Change";
            this.buttonChange3.UseVisualStyleBackColor = true;
            this.buttonChange3.Click += new System.EventHandler(this.buttonChange3_Click);
            // 
            // buttonChange2
            // 
            this.buttonChange2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange2.Location = new System.Drawing.Point(11, 285);
            this.buttonChange2.Name = "buttonChange2";
            this.buttonChange2.Size = new System.Drawing.Size(74, 32);
            this.buttonChange2.TabIndex = 16;
            this.buttonChange2.TabStop = false;
            this.buttonChange2.Text = "Change";
            this.buttonChange2.UseVisualStyleBackColor = true;
            this.buttonChange2.Click += new System.EventHandler(this.buttonChange2_Click);
            // 
            // buttonChange1
            // 
            this.buttonChange1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange1.Location = new System.Drawing.Point(11, 141);
            this.buttonChange1.Name = "buttonChange1";
            this.buttonChange1.Size = new System.Drawing.Size(74, 32);
            this.buttonChange1.TabIndex = 15;
            this.buttonChange1.TabStop = false;
            this.buttonChange1.Text = "Change";
            this.buttonChange1.UseVisualStyleBackColor = true;
            this.buttonChange1.Click += new System.EventHandler(this.buttonChange1_Click);
            // 
            // labelRatingTitle3
            // 
            this.labelRatingTitle3.AutoSize = true;
            this.labelRatingTitle3.Location = new System.Drawing.Point(105, 401);
            this.labelRatingTitle3.Name = "labelRatingTitle3";
            this.labelRatingTitle3.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle3.TabIndex = 10;
            this.labelRatingTitle3.Text = "Rating:";
            // 
            // labelName3
            // 
            this.labelName3.AutoSize = true;
            this.labelName3.Location = new System.Drawing.Point(104, 329);
            this.labelName3.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName3.Name = "labelName3";
            this.labelName3.Size = new System.Drawing.Size(60, 24);
            this.labelName3.TabIndex = 9;
            this.labelName3.Text = "label1";
            // 
            // labelRatingTitle2
            // 
            this.labelRatingTitle2.AutoSize = true;
            this.labelRatingTitle2.Location = new System.Drawing.Point(105, 257);
            this.labelRatingTitle2.Name = "labelRatingTitle2";
            this.labelRatingTitle2.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle2.TabIndex = 8;
            this.labelRatingTitle2.Text = "Rating:";
            // 
            // labelName2
            // 
            this.labelName2.AutoSize = true;
            this.labelName2.Location = new System.Drawing.Point(104, 183);
            this.labelName2.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName2.Name = "labelName2";
            this.labelName2.Size = new System.Drawing.Size(60, 24);
            this.labelName2.TabIndex = 7;
            this.labelName2.Text = "label1";
            // 
            // labelRatingTitle1
            // 
            this.labelRatingTitle1.AutoSize = true;
            this.labelRatingTitle1.Location = new System.Drawing.Point(104, 111);
            this.labelRatingTitle1.Name = "labelRatingTitle1";
            this.labelRatingTitle1.Size = new System.Drawing.Size(68, 24);
            this.labelRatingTitle1.TabIndex = 6;
            this.labelRatingTitle1.Text = "Rating:";
            // 
            // labelName1
            // 
            this.labelName1.AutoSize = true;
            this.labelName1.Location = new System.Drawing.Point(104, 39);
            this.labelName1.MaximumSize = new System.Drawing.Size(300, 0);
            this.labelName1.Name = "labelName1";
            this.labelName1.Size = new System.Drawing.Size(60, 24);
            this.labelName1.TabIndex = 5;
            this.labelName1.Text = "label1";
            // 
            // pictureBoxRecipe3
            // 
            this.pictureBoxRecipe3.Location = new System.Drawing.Point(11, 329);
            this.pictureBoxRecipe3.Name = "pictureBoxRecipe3";
            this.pictureBoxRecipe3.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe3.TabIndex = 2;
            this.pictureBoxRecipe3.TabStop = false;
            // 
            // pictureBoxRecipe2
            // 
            this.pictureBoxRecipe2.Location = new System.Drawing.Point(11, 183);
            this.pictureBoxRecipe2.Name = "pictureBoxRecipe2";
            this.pictureBoxRecipe2.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe2.TabIndex = 1;
            this.pictureBoxRecipe2.TabStop = false;
            // 
            // pictureBoxRecipe1
            // 
            this.pictureBoxRecipe1.Location = new System.Drawing.Point(11, 39);
            this.pictureBoxRecipe1.Name = "pictureBoxRecipe1";
            this.pictureBoxRecipe1.Size = new System.Drawing.Size(74, 96);
            this.pictureBoxRecipe1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecipe1.TabIndex = 0;
            this.pictureBoxRecipe1.TabStop = false;
            // 
            // MyRatingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 523);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrevious);
            this.Controls.Add(this.groupBoxRatings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MyRatingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yummy";
            this.groupBoxRatings.ResumeLayout(false);
            this.groupBoxRatings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecipe1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.GroupBox groupBoxRatings;
        private System.Windows.Forms.Label labelRating6;
        private System.Windows.Forms.Label labelRating5;
        private System.Windows.Forms.Label labelRating4;
        private System.Windows.Forms.Label labelRating3;
        private System.Windows.Forms.Label labelRating2;
        private System.Windows.Forms.Label labelRating1;
        private System.Windows.Forms.Button buttonChange6;
        private System.Windows.Forms.Button buttonChange5;
        private System.Windows.Forms.Button buttonChange4;
        private System.Windows.Forms.Label labelRatingTitle6;
        private System.Windows.Forms.Label labelName6;
        private System.Windows.Forms.Label labelRatingTitle5;
        private System.Windows.Forms.Label labelName5;
        private System.Windows.Forms.Label labelRatingTitle4;
        private System.Windows.Forms.Label labelName4;
        private System.Windows.Forms.PictureBox pictureBoxRecipe6;
        private System.Windows.Forms.PictureBox pictureBoxRecipe5;
        private System.Windows.Forms.PictureBox pictureBoxRecipe4;
        private System.Windows.Forms.Button buttonChange3;
        private System.Windows.Forms.Button buttonChange2;
        private System.Windows.Forms.Button buttonChange1;
        private System.Windows.Forms.Label labelRatingTitle3;
        private System.Windows.Forms.Label labelName3;
        private System.Windows.Forms.Label labelRatingTitle2;
        private System.Windows.Forms.Label labelName2;
        private System.Windows.Forms.Label labelRatingTitle1;
        private System.Windows.Forms.Label labelName1;
        private System.Windows.Forms.PictureBox pictureBoxRecipe3;
        private System.Windows.Forms.PictureBox pictureBoxRecipe2;
        private System.Windows.Forms.PictureBox pictureBoxRecipe1;
    }
}