﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recommender_System.Forms
{
    public partial class AddRecipeForm : Form
    {
        GraphClient client;

        public AddRecipeForm(GraphClient client)
        {
            InitializeComponent();
            this.client = client;
            fillListBox();
        }

        private void fillListBox()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Ingredient) return n",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
            List<Ingredient> ingredients = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList();

            foreach (Ingredient i in ingredients)
                listBox1.Items.Add(i.name);
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "" && typeComboBox.SelectedIndex != -1
                && textBoxTimeToCook.Text != "" && textBoxInstructions.Text != "" && listBox2.Items.Count > 0)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Recipe) WHERE n.name =~ '" + nameTextBox.Text + "' return n;",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
                Recipe check = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList().FirstOrDefault();

                if (check != null)
                    MessageBox.Show("Recipe with this name already exists!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    string maxId = getMaxId("Recipe");
                    int id;
                    try
                    {
                        int mId = int.Parse(maxId);
                        id = (++mId);
                    }
                    catch (Exception exception)
                    {
                        id = 1;
                    }

                    Recipe recipe = new Recipe(id, nameTextBox.Text, textBoxInstructions.Text,
                                              textBoxTimeToCook.Text, typeComboBox.SelectedItem.ToString());
                    recipe.createRecipe(client);

                    foreach (string name in listBox2.Items)
                        recipe.addIngredients(name, client);
                    
                    Close();
                }
            }
            else
                MessageBox.Show("Please fill in all the fields!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + s + ") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                listBox2.Items.Add(listBox1.SelectedItem);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                listBox1.SelectedIndex = -1;
            }
            else
                MessageBox.Show("Please select ingredient you wish to add first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                listBox1.Items.Add(listBox2.SelectedItem);
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                listBox2.SelectedIndex = -1;
            }
            else
                MessageBox.Show("Please select ingredient you wish to remove first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}