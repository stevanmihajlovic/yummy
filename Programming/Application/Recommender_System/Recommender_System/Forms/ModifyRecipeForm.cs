﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recommender_System.Forms
{
    public partial class ModifyRecipeForm : Form
    {
        GraphClient client;
        List<Recipe> recipes;
        List<Ingredient> ingredients;

        public ModifyRecipeForm(GraphClient client)
        {
            InitializeComponent();
            this.client = client;
            recipes = new List<Recipe>();
            ingredients = new List<Ingredient>();
            loadRecipes();
            loadIngredients();
        }

        private void loadRecipes()
        {
            recipes.Clear();

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Recipe) return n;",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            recipes = ((IRawGraphClient)client).ExecuteGetCypherResults<Recipe>(query).ToList();

            listBox3.Items.Clear();

            foreach (Recipe r in recipes)
            {
                listBox3.Items.Add(r.name);
                r.loadIngredients(client);
            }
        }

        private void loadIngredients()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Ingredient) return n;",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            ingredients = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList();
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();

            Recipe recipe = recipes[listBox3.SelectedIndex];

            nameTextBox.Text = recipe.name;
            typeComboBox.SelectedIndex = typeComboBox.Items.IndexOf(recipe.type);
            textBoxTimeToCook.Text = recipe.timeToPrepare;
            textBoxInstructions.Text = recipe.instructions;

            foreach (Ingredient i in ingredients)
                listBox1.Items.Add(i.name);

            foreach (Ingredient i in recipes[listBox3.SelectedIndex].ingredients)
            {
                listBox1.Items.Remove(i.name);
                listBox2.Items.Add(i.name);
            }
        }

        private void buttonModify_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text != "" && typeComboBox.SelectedIndex != -1 
                && textBoxTimeToCook.Text != "" && textBoxInstructions.Text != "" && listBox2.Items.Count > 0)
            {
                if (listBox3.SelectedIndex != -1)
                {
                    Recipe recipe = recipes[listBox3.SelectedIndex];

                    recipe.name = nameTextBox.Text;
                    recipe.instructions = textBoxInstructions.Text;
                    recipe.timeToPrepare = textBoxTimeToCook.Text;
                    recipe.type = typeComboBox.SelectedItem.ToString();

                    recipe.updateRecipe(client);

                    recipe.deleteIngredients(client);

                    foreach (string name in listBox2.Items)
                        recipe.addIngredients(name, client);

                    recipe.loadIngredients(client);

                    loadRecipes();

                    nameTextBox.Text = "";
                    typeComboBox.SelectedIndex = -1;
                    textBoxTimeToCook.Text = "";
                    textBoxInstructions.Text = "";
                    listBox1.Items.Clear();
                    listBox2.Items.Clear();

                    Close();
                }
                else
                    MessageBox.Show("Please select which recipe you wish to modify first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Please fill in all the fields!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                listBox2.Items.Add(listBox1.SelectedItem);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                listBox1.SelectedIndex = -1;
            }
            else
                MessageBox.Show("Please select ingredient you wish to add first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                listBox1.Items.Add(listBox2.SelectedItem);
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                listBox2.SelectedIndex = -1;
            }
            else
                MessageBox.Show("Please select ingredient you wish to remove first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listBox3.SelectedIndex != -1)
            {
                recipes[listBox3.SelectedIndex].deleteRecipe(client);

                loadRecipes();

                nameTextBox.Text = "";
                typeComboBox.SelectedIndex = -1;
                textBoxTimeToCook.Text = "";
                textBoxInstructions.Text = "";
                listBox1.Items.Clear();
                listBox2.Items.Clear();

                Close();
            }
            else
                MessageBox.Show("Please select recipe you wish to delete first!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
