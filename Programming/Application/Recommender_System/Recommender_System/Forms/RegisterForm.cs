﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Neo4jClient;
using Neo4jClient.Cypher;
using Recommender_System.Classes;
using Recommender_System.Forms;

namespace Recommender_System.Forms
{
    public partial class RegisterForm : Form
    {
        public GraphClient client;

        public RegisterForm()
        {
            InitializeComponent();
        }
        public RegisterForm(GraphClient client)
        {
            this.client = client;
            InitializeComponent();
        }
        private void registerButton_Click(object sender, EventArgs e)
        {
            if (usernameTextBox.Text != "" && passwordTextBox.Text != "" && firstnameTextBox.Text != ""
                && lastnameTextBox.Text != "" && ageTextBox.Text != "" && countryTextBox.Text != "")
            {
                int age;
                if (int.TryParse(ageTextBox.Text, out age))
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User) WHERE n.username =~ '"
                                                                    + usernameTextBox.Text + "' return n;",
                                                                    new Dictionary<string, object>(), CypherResultMode.Set);
                    User check = ((IRawGraphClient)client).ExecuteGetCypherResults<User>(query).ToList().FirstOrDefault();
                    if (check != null)
                        MessageBox.Show("User with this username already exists!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        string maxId = getMaxId("User");
                        int id;
                        try
                        {
                            int mId = int.Parse(maxId);
                            id = (++mId);
                        }
                        catch (Exception exception)
                        {
                            id = 1;
                        }

                        User user = new User(id, usernameTextBox.Text, passwordTextBox.Text, firstnameTextBox.Text,
                                            lastnameTextBox.Text, age, countryTextBox.Text);
                        user.createUser(client);

                        Hide();
                        SelectIngredientsForm form = new SelectIngredientsForm(client, "alergic", user);
                        form.ShowDialog();
                        Close();
                    }
                }
                else
                    MessageBox.Show("Please use only numbers for age!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Please fill in all the fields!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private String getMaxId(string s)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:" + s + ") where has(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
