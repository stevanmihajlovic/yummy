﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recommender_System.Classes
{
    public class User
    {
        public int id { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public int age { get; set; }    //ili bday ako nam je potrebno da gledamo slicne usere
        public String country { get; set; } //isto za slicne usere...

        public User()
        {

        }
        public User(int id,String username, String password, String firstName, String lastName, int age, String country)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.country = country;
        }

        public void createUser(GraphClient client)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("username", username);
            queryDict.Add("password", password);
            queryDict.Add("firstName", firstName);
            queryDict.Add("lastName", lastName);
            queryDict.Add("age", age);
            queryDict.Add("country", country);
            queryDict.Add("__type__", "org.neo4j.RS.domain.User");

            //create user - proveri apostrofe gde bese idu
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:User {id:" + id + ", username:'" + username
                                                            + "', password:'" + password +
                                                            "', firstName:'" + firstName +
                                                            "', lastName:'" + lastName + 
                                                            "', age:" + age + ", country:'" + country +
                                                            "', __type__:'org.neo4j.RS.domain.User'}) return n",
                                                            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }
    }
}
