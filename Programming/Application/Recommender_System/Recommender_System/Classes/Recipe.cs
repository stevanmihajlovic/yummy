﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recommender_System.Classes
{
    public class Recipe
    {
        public int id { get; set; }
        public String name { get; set; }
        public String instructions { get; set; }
        public String timeToPrepare { get; set; }
        public String type { get; set; }
        public List<Ingredient> ingredients { get; set; }

        public Recipe() {}

        public Recipe(int id, String name, String instructions, String timeToPrepare, String type)
        {
            this.id = id;
            this.name = name;
            this.instructions = instructions;
            this.timeToPrepare = timeToPrepare;
            this.type = type;
            ingredients = new List<Ingredient>();
        }

        public void loadIngredients(GraphClient client)
        {
            if (ingredients != null)
                ingredients.Clear();
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a: Recipe), (b:Ingredient), b-[r: partOf]->a WHERE a.name =~ '"
                                                            + name + "' return b",
                                                            new Dictionary<string, object>(),
                                                            CypherResultMode.Set);
            ingredients = ((IRawGraphClient)client).ExecuteGetCypherResults<Ingredient>(query).ToList();
        }

        public void createRecipe(GraphClient client)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("name", name);
            queryDict.Add("type", type);
            queryDict.Add("instructions", instructions);
            queryDict.Add("timeToPrepare", timeToPrepare);           
            queryDict.Add("__type__", "org.neo4j.RS.domain.Recipe");

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Recipe {id:" + id + ", name:'" + name
                                                            + "', type:'" + type + "', instructions:'" + instructions
                                                            + "', timeToPrepare:'" + timeToPrepare
                                                            + "', __type__:'org.neo4j.RS.domain.Recipe'}) return n",
                                                            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }
        
        public void updateRecipe(GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Recipe) where n.id = " + id + " set n.name = '" + name
                                                            + "', n.type = '" + type + "', n.instructions='" + instructions
                                                            + "', n.timeToPrepare = '" + timeToPrepare
                                                            + "' return n",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

        }

        public void deleteRecipe(GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Recipe) where n.id = " + id + " detach delete n",
                                                            new Dictionary<string, object>(), CypherResultMode.Projection);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        public void addIngredients(String ingredientName, GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a: Recipe), (b:Ingredient) WHERE a.name =~ '" + name
                                                                        + "' AND b.name =~ '" + ingredientName +
                                                                        "' CREATE(b)-[r:partOf]->(a) RETURN r",
                                                                        new Dictionary<string, object>(),
                                                                        CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        public void deleteIngredients(GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a: Recipe), (b:Ingredient), b-[r: partOf]->a WHERE a.name =~ '"
                                                            + name + "' delete r",
                                                            new Dictionary<string, object>(),
                                                            CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

    }
}
