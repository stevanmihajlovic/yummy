﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recommender_System.Classes
{
    public class Ingredient
    {
        public int id { get; set; }
        public String name { get; set; }
        public String type { get; set; }

        public Ingredient() { }
        public Ingredient(int id, String name, String type)
        {
            this.id = id;
            this.name = name;
            this.type = type;
        }

        public void createIngredient(GraphClient client)
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("type", type);
            queryDict.Add("name", name);

            queryDict.Add("__type__", "org.neo4j.RS.domain.Ingredient");
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Ingredient {id:" + id + ", name:'" + name
                                                            + "', type:'" + type
                                                            + "', __type__:'org.neo4j.RS.domain.Ingredient'}) return n",
                                                            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        public void updateIngredient(GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Ingredient) where n.id = " + id + " set n.name = '" + name
                                                                    + "', n.type='" + type + "' return n",
                                                                    new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

        }

        public void deleteIngredient(GraphClient client)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Ingredient) where n.id = " + id + " delete n",
                                                        new Dictionary<string, object>(), CypherResultMode.Projection);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }
    }
}
